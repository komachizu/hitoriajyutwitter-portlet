<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="javax.portlet.ActionRequest" %>
<%@ page import="twitter4j.Status" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="javax.portlet.PortletPreferences" %>

<%-- お試しCSS 
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.2/jquery.mobile-1.4.2.min.css" />
--%>

<portlet:defineObjects />

<script type="text/javascript" src="/HitoriajyuTwitter-portlet/js/jquery-2.1.4.js"></script>
<script type="text/javascript" src="/HitoriajyuTwitter-portlet/js/jquery.balloon.min.js"></script>
<aui:script use="aui-base">
	var userIdVal = 1000;

	AUI().ready(
		function(){
			console.log("init!!!");
			viewListPage(userIdVal);
		}
	);

	Liferay.bind('goListPage',function(event,data){
		console.log("bind!! goListPage");
		$("#listPage").show();
		$("#retweetPage").hide();
		$("#tweetPage").hide();	
		return false;
	});

	Liferay.bind('goRetweetPage',function(event,data){
		console.log("goRetweetPage!!");
		var tweet = JSON.parse(data);
		var html = "";
		html += "<div>";
		html += "<img class='image-left' src='/HitoriajyuTwitter-portlet/images/hitoria_sample75.jpg'>";
        html += "<div class='arrow-box-left-container'>";
        html += "<div class='arrow-box-left'>";
		html += tweet.tweetVal;
        html += "</div>";
        html += "</div>";
		html += "</div>";   

		html += "<hr class='clear' />";
		html += "<h3>リツイート一覧</h3>";

		Liferay.Service(
		  '/HitoriajyuTwitter-portlet.retweet/get-re-tweet-list',
		  {
		    tweetId: tweet.tweetId
		  },
		  function(obj) {
		    console.log(obj);

		    html += "<ul class='ui-content' >";
		    for(i=0; i < obj.length ;i++){
		      var tweet = obj[i];
		      if (!tweet.retweet) {
		        continue;
		      }
		      html += "<li class='tweet'>";
		      html += "<hr class='clear'/>";
		      if ((i % 2) === 0) {
		        html += "<div class='arrow-box-right-container'>";
		        html += "<div class='arrow-box-right'>";
		        html += tweet.retweet;
		        html += "</div>";
		        html += "</div>";
		        html += "<img class='image-right' src='" + tweet.icon + "'>";
		      } else {
		        html += "<img class='image-right' src='" + tweet.icon + "'>";
		        html += "<div class='arrow-box-left-container'>";
		        html += "<div class='arrow-box-left'>";
		        html += tweet.retweet;
		        html += "</div>";
		        html += "</div>";
		      }
		      html += "</li>";
		    }
		    html += "</ul>";
		    $("#retweetPage").html(html);
		  }
		);

	    $("#listPage").hide();
		$("#retweetPage").show();
		$("#tweetPage").hide();	

		return false;
	});
	
	Liferay.bind('goTweetPage',function(event,data){
		console.log("bind!! goTweetPage");
		$("#listPage").hide();
		$("#retweetPage").hide();
		$("#tweetPage").show();
		return false;
	});
</aui:script>
<script type="text/javascript"> 
	/**
	 * @param {string} query
	 */
	function setKeyword(query) {
		console.log(query);
		   $.ajax({
		     dataType: "jsonp",
		     data: {
		       "appid": "dj0zaiZpPVF4T2dRckZNR2N6MSZzPWNvbnN1bWVyc2VjcmV0Jng9YzM-",
		       "sentence": query,
		       "output" : "json",
		       "max-results":"10",
		       "alt":"json-in-script"
		     },
		     cache: true,
		     url: "http://jlp.yahooapis.jp/KeyphraseService/V1/extract",
		     success: function (data) {
				var keyword = '';
				var init = true;
				for(key in data){
					console.log(key);
					if(init){
						keyword = keyword + key;
						init = false;
					}else{
						keyword = keyword + ',' +key;
					}
				}
							
				$('#_twittergettertest_WAR_HitoriajyuTwitterportlet_tweet_keyword').val(keyword);
				Liferay.Service(
						  '/HitoriajyuTwitter-portlet.tweet/exec-tweet',
						  {
						    userId: 1000,
						    tweetVal: query,
						    keywordCSV: keyword
						  },
						  function(obj) {

							var tweet = 
							{ 
							  tweetId:query,	
							  tweetVal:obj	
							};
							
							var tweet_text = JSON.stringify(tweet);
						    Liferay.trigger('goRetweetPage',tweet_text);
						  }
						);
		     }
		   });
	}
	
	/**
	 * @param tweetId
	 * @param {string} tweetVal
	 */
	function selectTweet(tweetId,tweetVal){
		console.log("clickTweet tweetId = " + tweetId + ":" + tweetVal);
		
		var tweet = 
			{ 
			  tweetId:tweetId,	
			  tweetVal:tweetVal	
			};
		
		var tweet_text = JSON.stringify(tweet);
		
		console.log(tweet_text);
		Liferay.trigger('goRetweetPage',tweet_text);
	}
	
	function submitTweet(){
		setKeyword($('#_twittergettertest_WAR_HitoriajyuTwitterportlet_tweet').val());
	}
	function viewListPage(userIdVal){
		$("#listPage").show();
		$("#retweetPage").hide();
		$("#tweetPage").hide();

		Liferay.Service(
		  '/HitoriajyuTwitter-portlet.tweet/get-tweet-list',
		  {
		    userId: userIdVal
		  },
		  function(obj) {
		    console.log(obj);

		    var html = "";

		    html += "<ul class='ui-content' >";
		    for(i=0; i < obj.length ;i++){
		    	var tweet = obj[i];
		    	if (!tweet.tweet) {
		    		continue;
		    	}
				html += "<li class='tweet'>";
				html += "<hr class='clear'/>";
				html += "<a href='javascript:selectTweet(" + tweet.tweetId +",\"" + tweet.tweet + "\")'>";
				html += "<img class='image-left' src='/HitoriajyuTwitter-portlet/images/hitoria_sample75.jpg'>";
				html += "<div class='arrow-box-left-container'>";
				html += "<div class='arrow-box-left'>";
				html += tweet.tweet;	
				html += "</div>";
				html += "</div>";
				html += "</a>";
				html += "</li>";
		    }
		    html += "</ul>";

			$("#listPage").html(html);
		  }
		);

	}
</script> 

<%-- 一覧画面 --%>
<div id="listPage">
	<div id="listPage_indicator"></div>
</div>


<%-- リツイート表示画面 --%>
<div id="retweetPage">

</div>

<%-- 発言画面 --%>
<div id="tweetPage">
	<img src='/HitoriajyuTwitter-portlet/images/hitoria_sample75.jpg'>
	<aui:form id="tweetForm">
		<aui:input id="tweet" label="" name="tweet" type="textarea" rows="5" cols="30" value="" />
		<aui:input id="tweet_keyword" label="分割キーワード" name="tweet_keyword" type="hidden" value="" />
		<%--
			<button class="btn btn-primary" onClick="javascript:submitTweet()">つぶやく</button>
		 --%>	
	</aui:form>
	<a href="javascript:submitTweet()">つぶやく</a>
	<br><br>
	Web Services by Yahoo! JAPAN
</div>