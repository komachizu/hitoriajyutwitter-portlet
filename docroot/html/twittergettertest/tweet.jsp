<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="javax.portlet.ActionRequest" %>
<%@ page import="twitter4j.Status" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="javax.portlet.PortletPreferences" %>

<portlet:defineObjects />
<script type="text/javascript" src="/HitoriajyuTwitter-portlet/js/jquery-2.1.4.js"></script>

<portlet:actionURL var="retweetURL">
 <portlet:param name="mvcPath" value="/html/twittergettertest/retweet.jsp" />
</portlet:actionURL>

<aui:form id="tweetForm" action="<%= retweetURL %>" method="post">
	<aui:input id="tweet" label="つぶやいてみる" name="tweet" type="textarea" rows="5" cols="30" value="" />
	<aui:input id="tweet_keyword" name="tweet_keyword" type="text" value="" />
	<button class="btn btn-primary" onClick="javascript:submitForm()">つぶやく</button>	
</aui:form>