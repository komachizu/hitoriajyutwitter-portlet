<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<portlet:defineObjects />
<script type="text/javascript" src="/HitoriajyuTwitter-portlet/js/jquery-2.1.4.js"></script>

<script type="text/javascript">

$('#tweet_textbox').on("click", function(){
	console.log("textbox click!!")
});

function openPage(pageName){
	console.log(pageName);
	return false;
}

function tweet(){
	console.log("tweet click!!");
	Liferay.trigger('goTweetPage','');
    return false;
}

function back(){
	console.log("back click!!");
	Liferay.trigger('goListPage','');
	
    return false;
}
</script>

<aui:nav-bar>
	<aui:nav cssClass="pull-left">
		<aui:nav-item id="nav-back" href="javascript:back()" iconCssClass="icon-arrow-left"/>
		<aui:nav-item id="nav-tweet" href="javascript:tweet()" label="なにをしている？" />
	</aui:nav>
	
	<aui:nav cssClass="pull-right">
		<aui:nav-item dropdown="<%= true %>" iconCssClass="icon-align-justify">
			<aui:nav-item href="javascript:openPage('paramater')" label="パラメータ設定" />
			<aui:nav-item href="javascript:openPage('status')" label="ステータス" />
			<aui:nav-item href="javascript:openPage('account')" label="アカウント" />
			<aui:nav-item href="javascript:openPage('help')" label="ヘルプ" />
		</aui:nav-item>
	</aui:nav>
</aui:nav-bar>