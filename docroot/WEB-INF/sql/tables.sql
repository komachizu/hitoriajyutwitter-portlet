create table hitoria_ReTweet (
	retweetId LONG not null primary key,
	tweetId LONG,
	retweet VARCHAR(1000) null,
	icon VARCHAR(1000) null,
	retweetDate DATE null,
	createDate DATE null,
	modifiedDate DATE null
);

create table hitoria_Tweet (
	tweetId LONG not null primary key,
	tweet VARCHAR(1000) null,
	botTweet BOOLEAN,
	userId LONG,
	createDate DATE null,
	modifiedDate DATE null
);

create table hitoria_retweetArc (
	retweetId LONG not null,
	keyword VARCHAR(75) not null,
	retweet VARCHAR(1000) null,
	icon VARCHAR(1000) null,
	retweetDate DATE null,
	createDate DATE null,
	modifiedDate DATE null,
	primary key (retweetId, keyword)
);