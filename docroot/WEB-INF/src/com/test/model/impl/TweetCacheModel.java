/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.test.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import com.test.model.Tweet;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Tweet in entity cache.
 *
 * @author takuro
 * @see Tweet
 * @generated
 */
public class TweetCacheModel implements CacheModel<Tweet>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{tweetId=");
		sb.append(tweetId);
		sb.append(", tweet=");
		sb.append(tweet);
		sb.append(", botTweet=");
		sb.append(botTweet);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Tweet toEntityModel() {
		TweetImpl tweetImpl = new TweetImpl();

		tweetImpl.setTweetId(tweetId);

		if (tweet == null) {
			tweetImpl.setTweet(StringPool.BLANK);
		}
		else {
			tweetImpl.setTweet(tweet);
		}

		tweetImpl.setBotTweet(botTweet);
		tweetImpl.setUserId(userId);

		if (createDate == Long.MIN_VALUE) {
			tweetImpl.setCreateDate(null);
		}
		else {
			tweetImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			tweetImpl.setModifiedDate(null);
		}
		else {
			tweetImpl.setModifiedDate(new Date(modifiedDate));
		}

		tweetImpl.resetOriginalValues();

		return tweetImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		tweetId = objectInput.readLong();
		tweet = objectInput.readUTF();
		botTweet = objectInput.readBoolean();
		userId = objectInput.readLong();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(tweetId);

		if (tweet == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tweet);
		}

		objectOutput.writeBoolean(botTweet);
		objectOutput.writeLong(userId);
		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);
	}

	public long tweetId;
	public String tweet;
	public boolean botTweet;
	public long userId;
	public long createDate;
	public long modifiedDate;
}