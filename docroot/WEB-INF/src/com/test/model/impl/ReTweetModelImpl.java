/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.test.model.impl;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.json.JSON;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;
import com.liferay.portlet.expando.util.ExpandoBridgeFactoryUtil;

import com.test.model.ReTweet;
import com.test.model.ReTweetModel;
import com.test.model.ReTweetSoap;

import java.io.Serializable;

import java.sql.Types;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The base model implementation for the ReTweet service. Represents a row in the &quot;hitoria_ReTweet&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface {@link com.test.model.ReTweetModel} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link ReTweetImpl}.
 * </p>
 *
 * @author takuro
 * @see ReTweetImpl
 * @see com.test.model.ReTweet
 * @see com.test.model.ReTweetModel
 * @generated
 */
@JSON(strict = true)
public class ReTweetModelImpl extends BaseModelImpl<ReTweet>
	implements ReTweetModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a re tweet model instance should use the {@link com.test.model.ReTweet} interface instead.
	 */
	public static final String TABLE_NAME = "hitoria_ReTweet";
	public static final Object[][] TABLE_COLUMNS = {
			{ "retweetId", Types.BIGINT },
			{ "tweetId", Types.BIGINT },
			{ "retweet", Types.VARCHAR },
			{ "icon", Types.VARCHAR },
			{ "retweetDate", Types.TIMESTAMP },
			{ "createDate", Types.TIMESTAMP },
			{ "modifiedDate", Types.TIMESTAMP }
		};
	public static final String TABLE_SQL_CREATE = "create table hitoria_ReTweet (retweetId LONG not null primary key,tweetId LONG,retweet VARCHAR(1000) null,icon VARCHAR(1000) null,retweetDate DATE null,createDate DATE null,modifiedDate DATE null)";
	public static final String TABLE_SQL_DROP = "drop table hitoria_ReTweet";
	public static final String ORDER_BY_JPQL = " ORDER BY reTweet.retweetDate DESC";
	public static final String ORDER_BY_SQL = " ORDER BY hitoria_ReTweet.retweetDate DESC";
	public static final String DATA_SOURCE = "liferayDataSource";
	public static final String SESSION_FACTORY = "liferaySessionFactory";
	public static final String TX_MANAGER = "liferayTransactionManager";
	public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.entity.cache.enabled.com.test.model.ReTweet"),
			true);
	public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.finder.cache.enabled.com.test.model.ReTweet"),
			true);
	public static final boolean COLUMN_BITMASK_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.column.bitmask.enabled.com.test.model.ReTweet"),
			true);
	public static long TWEETID_COLUMN_BITMASK = 1L;
	public static long RETWEETDATE_COLUMN_BITMASK = 2L;

	/**
	 * Converts the soap model instance into a normal model instance.
	 *
	 * @param soapModel the soap model instance to convert
	 * @return the normal model instance
	 */
	public static ReTweet toModel(ReTweetSoap soapModel) {
		if (soapModel == null) {
			return null;
		}

		ReTweet model = new ReTweetImpl();

		model.setRetweetId(soapModel.getRetweetId());
		model.setTweetId(soapModel.getTweetId());
		model.setRetweet(soapModel.getRetweet());
		model.setIcon(soapModel.getIcon());
		model.setRetweetDate(soapModel.getRetweetDate());
		model.setCreateDate(soapModel.getCreateDate());
		model.setModifiedDate(soapModel.getModifiedDate());

		return model;
	}

	/**
	 * Converts the soap model instances into normal model instances.
	 *
	 * @param soapModels the soap model instances to convert
	 * @return the normal model instances
	 */
	public static List<ReTweet> toModels(ReTweetSoap[] soapModels) {
		if (soapModels == null) {
			return null;
		}

		List<ReTweet> models = new ArrayList<ReTweet>(soapModels.length);

		for (ReTweetSoap soapModel : soapModels) {
			models.add(toModel(soapModel));
		}

		return models;
	}

	public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
				"lock.expiration.time.com.test.model.ReTweet"));

	public ReTweetModelImpl() {
	}

	@Override
	public long getPrimaryKey() {
		return _retweetId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setRetweetId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _retweetId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Class<?> getModelClass() {
		return ReTweet.class;
	}

	@Override
	public String getModelClassName() {
		return ReTweet.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("retweetId", getRetweetId());
		attributes.put("tweetId", getTweetId());
		attributes.put("retweet", getRetweet());
		attributes.put("icon", getIcon());
		attributes.put("retweetDate", getRetweetDate());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long retweetId = (Long)attributes.get("retweetId");

		if (retweetId != null) {
			setRetweetId(retweetId);
		}

		Long tweetId = (Long)attributes.get("tweetId");

		if (tweetId != null) {
			setTweetId(tweetId);
		}

		String retweet = (String)attributes.get("retweet");

		if (retweet != null) {
			setRetweet(retweet);
		}

		String icon = (String)attributes.get("icon");

		if (icon != null) {
			setIcon(icon);
		}

		Date retweetDate = (Date)attributes.get("retweetDate");

		if (retweetDate != null) {
			setRetweetDate(retweetDate);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}
	}

	@JSON
	@Override
	public long getRetweetId() {
		return _retweetId;
	}

	@Override
	public void setRetweetId(long retweetId) {
		_retweetId = retweetId;
	}

	@JSON
	@Override
	public long getTweetId() {
		return _tweetId;
	}

	@Override
	public void setTweetId(long tweetId) {
		_columnBitmask |= TWEETID_COLUMN_BITMASK;

		if (!_setOriginalTweetId) {
			_setOriginalTweetId = true;

			_originalTweetId = _tweetId;
		}

		_tweetId = tweetId;
	}

	public long getOriginalTweetId() {
		return _originalTweetId;
	}

	@JSON
	@Override
	public String getRetweet() {
		if (_retweet == null) {
			return StringPool.BLANK;
		}
		else {
			return _retweet;
		}
	}

	@Override
	public void setRetweet(String retweet) {
		_retweet = retweet;
	}

	@JSON
	@Override
	public String getIcon() {
		if (_icon == null) {
			return StringPool.BLANK;
		}
		else {
			return _icon;
		}
	}

	@Override
	public void setIcon(String icon) {
		_icon = icon;
	}

	@JSON
	@Override
	public Date getRetweetDate() {
		return _retweetDate;
	}

	@Override
	public void setRetweetDate(Date retweetDate) {
		_columnBitmask = -1L;

		_retweetDate = retweetDate;
	}

	@JSON
	@Override
	public Date getCreateDate() {
		return _createDate;
	}

	@Override
	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	@JSON
	@Override
	public Date getModifiedDate() {
		return _modifiedDate;
	}

	@Override
	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getColumnBitmask() {
		return _columnBitmask;
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return ExpandoBridgeFactoryUtil.getExpandoBridge(0,
			ReTweet.class.getName(), getPrimaryKey());
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		ExpandoBridge expandoBridge = getExpandoBridge();

		expandoBridge.setAttributes(serviceContext);
	}

	@Override
	public ReTweet toEscapedModel() {
		if (_escapedModel == null) {
			_escapedModel = (ReTweet)ProxyUtil.newProxyInstance(_classLoader,
					_escapedModelInterfaces, new AutoEscapeBeanHandler(this));
		}

		return _escapedModel;
	}

	@Override
	public Object clone() {
		ReTweetImpl reTweetImpl = new ReTweetImpl();

		reTweetImpl.setRetweetId(getRetweetId());
		reTweetImpl.setTweetId(getTweetId());
		reTweetImpl.setRetweet(getRetweet());
		reTweetImpl.setIcon(getIcon());
		reTweetImpl.setRetweetDate(getRetweetDate());
		reTweetImpl.setCreateDate(getCreateDate());
		reTweetImpl.setModifiedDate(getModifiedDate());

		reTweetImpl.resetOriginalValues();

		return reTweetImpl;
	}

	@Override
	public int compareTo(ReTweet reTweet) {
		int value = 0;

		value = DateUtil.compareTo(getRetweetDate(), reTweet.getRetweetDate());

		value = value * -1;

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ReTweet)) {
			return false;
		}

		ReTweet reTweet = (ReTweet)obj;

		long primaryKey = reTweet.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public void resetOriginalValues() {
		ReTweetModelImpl reTweetModelImpl = this;

		reTweetModelImpl._originalTweetId = reTweetModelImpl._tweetId;

		reTweetModelImpl._setOriginalTweetId = false;

		reTweetModelImpl._columnBitmask = 0;
	}

	@Override
	public CacheModel<ReTweet> toCacheModel() {
		ReTweetCacheModel reTweetCacheModel = new ReTweetCacheModel();

		reTweetCacheModel.retweetId = getRetweetId();

		reTweetCacheModel.tweetId = getTweetId();

		reTweetCacheModel.retweet = getRetweet();

		String retweet = reTweetCacheModel.retweet;

		if ((retweet != null) && (retweet.length() == 0)) {
			reTweetCacheModel.retweet = null;
		}

		reTweetCacheModel.icon = getIcon();

		String icon = reTweetCacheModel.icon;

		if ((icon != null) && (icon.length() == 0)) {
			reTweetCacheModel.icon = null;
		}

		Date retweetDate = getRetweetDate();

		if (retweetDate != null) {
			reTweetCacheModel.retweetDate = retweetDate.getTime();
		}
		else {
			reTweetCacheModel.retweetDate = Long.MIN_VALUE;
		}

		Date createDate = getCreateDate();

		if (createDate != null) {
			reTweetCacheModel.createDate = createDate.getTime();
		}
		else {
			reTweetCacheModel.createDate = Long.MIN_VALUE;
		}

		Date modifiedDate = getModifiedDate();

		if (modifiedDate != null) {
			reTweetCacheModel.modifiedDate = modifiedDate.getTime();
		}
		else {
			reTweetCacheModel.modifiedDate = Long.MIN_VALUE;
		}

		return reTweetCacheModel;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{retweetId=");
		sb.append(getRetweetId());
		sb.append(", tweetId=");
		sb.append(getTweetId());
		sb.append(", retweet=");
		sb.append(getRetweet());
		sb.append(", icon=");
		sb.append(getIcon());
		sb.append(", retweetDate=");
		sb.append(getRetweetDate());
		sb.append(", createDate=");
		sb.append(getCreateDate());
		sb.append(", modifiedDate=");
		sb.append(getModifiedDate());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(25);

		sb.append("<model><model-name>");
		sb.append("com.test.model.ReTweet");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>retweetId</column-name><column-value><![CDATA[");
		sb.append(getRetweetId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tweetId</column-name><column-value><![CDATA[");
		sb.append(getTweetId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>retweet</column-name><column-value><![CDATA[");
		sb.append(getRetweet());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>icon</column-name><column-value><![CDATA[");
		sb.append(getIcon());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>retweetDate</column-name><column-value><![CDATA[");
		sb.append(getRetweetDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>createDate</column-name><column-value><![CDATA[");
		sb.append(getCreateDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
		sb.append(getModifiedDate());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private static ClassLoader _classLoader = ReTweet.class.getClassLoader();
	private static Class<?>[] _escapedModelInterfaces = new Class[] {
			ReTweet.class
		};
	private long _retweetId;
	private long _tweetId;
	private long _originalTweetId;
	private boolean _setOriginalTweetId;
	private String _retweet;
	private String _icon;
	private Date _retweetDate;
	private Date _createDate;
	private Date _modifiedDate;
	private long _columnBitmask;
	private ReTweet _escapedModel;
}