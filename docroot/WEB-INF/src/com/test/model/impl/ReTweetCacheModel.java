/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.test.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import com.test.model.ReTweet;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing ReTweet in entity cache.
 *
 * @author takuro
 * @see ReTweet
 * @generated
 */
public class ReTweetCacheModel implements CacheModel<ReTweet>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{retweetId=");
		sb.append(retweetId);
		sb.append(", tweetId=");
		sb.append(tweetId);
		sb.append(", retweet=");
		sb.append(retweet);
		sb.append(", icon=");
		sb.append(icon);
		sb.append(", retweetDate=");
		sb.append(retweetDate);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ReTweet toEntityModel() {
		ReTweetImpl reTweetImpl = new ReTweetImpl();

		reTweetImpl.setRetweetId(retweetId);
		reTweetImpl.setTweetId(tweetId);

		if (retweet == null) {
			reTweetImpl.setRetweet(StringPool.BLANK);
		}
		else {
			reTweetImpl.setRetweet(retweet);
		}

		if (icon == null) {
			reTweetImpl.setIcon(StringPool.BLANK);
		}
		else {
			reTweetImpl.setIcon(icon);
		}

		if (retweetDate == Long.MIN_VALUE) {
			reTweetImpl.setRetweetDate(null);
		}
		else {
			reTweetImpl.setRetweetDate(new Date(retweetDate));
		}

		if (createDate == Long.MIN_VALUE) {
			reTweetImpl.setCreateDate(null);
		}
		else {
			reTweetImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			reTweetImpl.setModifiedDate(null);
		}
		else {
			reTweetImpl.setModifiedDate(new Date(modifiedDate));
		}

		reTweetImpl.resetOriginalValues();

		return reTweetImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		retweetId = objectInput.readLong();
		tweetId = objectInput.readLong();
		retweet = objectInput.readUTF();
		icon = objectInput.readUTF();
		retweetDate = objectInput.readLong();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(retweetId);
		objectOutput.writeLong(tweetId);

		if (retweet == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(retweet);
		}

		if (icon == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(icon);
		}

		objectOutput.writeLong(retweetDate);
		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);
	}

	public long retweetId;
	public long tweetId;
	public String retweet;
	public String icon;
	public long retweetDate;
	public long createDate;
	public long modifiedDate;
}