/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.test.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import com.test.model.retweetArc;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing retweetArc in entity cache.
 *
 * @author takuro
 * @see retweetArc
 * @generated
 */
public class retweetArcCacheModel implements CacheModel<retweetArc>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{retweetId=");
		sb.append(retweetId);
		sb.append(", keyword=");
		sb.append(keyword);
		sb.append(", retweet=");
		sb.append(retweet);
		sb.append(", icon=");
		sb.append(icon);
		sb.append(", retweetDate=");
		sb.append(retweetDate);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public retweetArc toEntityModel() {
		retweetArcImpl retweetArcImpl = new retweetArcImpl();

		retweetArcImpl.setRetweetId(retweetId);

		if (keyword == null) {
			retweetArcImpl.setKeyword(StringPool.BLANK);
		}
		else {
			retweetArcImpl.setKeyword(keyword);
		}

		if (retweet == null) {
			retweetArcImpl.setRetweet(StringPool.BLANK);
		}
		else {
			retweetArcImpl.setRetweet(retweet);
		}

		if (icon == null) {
			retweetArcImpl.setIcon(StringPool.BLANK);
		}
		else {
			retweetArcImpl.setIcon(icon);
		}

		if (retweetDate == Long.MIN_VALUE) {
			retweetArcImpl.setRetweetDate(null);
		}
		else {
			retweetArcImpl.setRetweetDate(new Date(retweetDate));
		}

		if (createDate == Long.MIN_VALUE) {
			retweetArcImpl.setCreateDate(null);
		}
		else {
			retweetArcImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			retweetArcImpl.setModifiedDate(null);
		}
		else {
			retweetArcImpl.setModifiedDate(new Date(modifiedDate));
		}

		retweetArcImpl.resetOriginalValues();

		return retweetArcImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		retweetId = objectInput.readLong();
		keyword = objectInput.readUTF();
		retweet = objectInput.readUTF();
		icon = objectInput.readUTF();
		retweetDate = objectInput.readLong();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(retweetId);

		if (keyword == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(keyword);
		}

		if (retweet == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(retweet);
		}

		if (icon == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(icon);
		}

		objectOutput.writeLong(retweetDate);
		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);
	}

	public long retweetId;
	public String keyword;
	public String retweet;
	public String icon;
	public long retweetDate;
	public long createDate;
	public long modifiedDate;
}