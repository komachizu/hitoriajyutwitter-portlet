package com.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.portlet.PortletClassLoaderUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.test.model.Tweet;
import com.test.service.TweetLocalServiceUtil;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Portlet implementation class TwitterGetterTest
 */
public class TwitterGetterTest extends MVCPortlet {
	
	private final static String CONSUMER_KEY =        "nJB3uWx8hyB5RTq1i3fuAKcp2";
	private final static String CONSUMER_SECRET =     "aObytIgQ8b4WKMvQ4KtURkhqH2SVPniXSvE0DjL4GqntzbFLvl";
	private final static String ACCESS_TOKEN =        "3301643772-F6offA8GFTDsGS9acC1XM7zUhyhwUPBbukz8Xaq";
	private final static String ACCESS_TOKEN_SECRET = "dQkZhdVkpKc00redP1GDRGuS4103teQyb90yUsvb5nyFL";

	@Override
	public void processAction(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {

		this.getTweetList(1000);
		
		PortletPreferences prefs = actionRequest.getPreferences();
        String tweet = actionRequest.getParameter("tweet");
        String tweet_keyword = actionRequest.getParameter("tweet_keyword");
        
        List<Status> retweetStatusList = new ArrayList<Status>(); 
        String[] keywords = tweet_keyword.split(",");

        Status ret = null;
        try {
	        for(String key:keywords){
				ret = getStatusRetweet(key);
				
	        	if(ret != null){
	        		retweetStatusList.add(ret);
	        	}
	        }
	
	        if (tweet != null) {
	            prefs.setValue("tweet", tweet);
	            prefs.setValue("tweet_keyword", tweet_keyword);
	            prefs.store();
	        }
        
        } catch (TwitterException e) {
			// TODO 自動生成された catch ブロック
        	System.out.println("利用制限オーバー？");
			actionRequest.setAttribute("massage", "ツイッターの利用制限に引っかかりました。しばらくお待ちください（一時間程度）");
		}
        System.out.println("返却リストサイズ:" + retweetStatusList.size());
		actionRequest.setAttribute("retweetStatusList", retweetStatusList);
        
		super.processAction(actionRequest, actionResponse);
	}

	@SuppressWarnings("unused")
	private Status getStatusRetweet(String keyword) throws TwitterException{
		System.out.println("keyword:" + keyword);
		Status retweetStatus = null;
		
        // 初期化・認証
		ConfigurationBuilder confbuilder = new ConfigurationBuilder();
		confbuilder.setDebugEnabled(true);
		confbuilder.setOAuthConsumerKey(CONSUMER_KEY);
		confbuilder.setOAuthConsumerSecret(CONSUMER_SECRET);
		confbuilder.setOAuthAccessToken(null);
		confbuilder.setOAuthAccessTokenSecret(null);
		TwitterFactory twitterfactory = new TwitterFactory(confbuilder.build());
		Twitter twitter = twitterfactory.getInstance();
		
//        Twitter twitter = new TwitterFactory().getInstance();
//        twitter.setOAuthConsumer(CONSUMER_KEY, CONSUMER_SECRET);
        AccessToken accessToken = new AccessToken(ACCESS_TOKEN, ACCESS_TOKEN_SECRET);
        twitter.setOAuthAccessToken(accessToken);
        
        
        Query query = new Query();
        query.setLang("ja");


        query.setCount(50);

        // 検索ワードをセット
        query.setQuery(keyword);
        
        // 検索実行
        QueryResult result;
		List<Status> resultList = new ArrayList<Status>();
        int retweetCount = 0;
		try {
	        
			result = twitter.search(query);
			
			System.out.println("@@@@limit!!:" + result.getRateLimitStatus());
			
			if(result != null){
				resultList = result.getTweets();
			}else{
				return retweetStatus;
			}
	        System.out.println("ヒット数 : " + resultList.size());

	        for (Status status : result.getTweets()) {
	        	
	        	// リツイート除外
	        	if(status.getRetweetedStatus() != null){
	        		retweetCount++;
	        		continue;
	        	}
       	
		        // リツイート取得
	            retweetStatus = this.getRetweet(status.getId());
	            if(retweetStatus != null){
	            	System.out.println("参照ツイート：" + status.getText());
	            	System.out.println("参照ツイートのリツイート：" + retweetStatus.getText());
	            	return retweetStatus;
	            }
	        }	        
		} catch (TwitterException e) {
			// TODO 自動生成された catch ブロック
			throw e;
		}
		return retweetStatus;
	}

	private Status getRetweet(long tweetId) throws TwitterException{
		Twitter twitter = new TwitterFactory().getInstance();
		try {
			
			List<Status> retweetList = twitter.getRetweets(tweetId);
			
			System.out.println("retweetListSize:" + retweetList.size());
			if(retweetList.size() > 0){
				return (Status)retweetList.get(0);
			}
			
		} catch (TwitterException e) {
			e.printStackTrace();
			throw e;
		}
		return null;
	}
	
	
	private List<Tweet> getTweetList(long userId){
		System.out.println("in getTweetList");
		
		DynamicQuery dq = DynamicQueryFactoryUtil.forClass(
				Tweet.class,PortletClassLoaderUtil.getClassLoader());
		dq.add(PropertyFactoryUtil.forName("userId").eq(userId));
		List<Tweet> ret = null;
		try {
			ret = TweetLocalServiceUtil.dynamicQuery(dq);
			
			for(Tweet tw:ret){
				System.out.println("tweetId:" + tw.getTweetId() + ", tweet:" + tw.getTweet());
				
			}
			return ret;
		} catch (SystemException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		
		
		return ret;
	}
}
