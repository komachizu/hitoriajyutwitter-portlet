/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.test.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import com.test.NoSuchReTweetException;

import com.test.model.ReTweet;
import com.test.model.impl.ReTweetImpl;
import com.test.model.impl.ReTweetModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the re tweet service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author takuro
 * @see ReTweetPersistence
 * @see ReTweetUtil
 * @generated
 */
public class ReTweetPersistenceImpl extends BasePersistenceImpl<ReTweet>
	implements ReTweetPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ReTweetUtil} to access the re tweet persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ReTweetImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ReTweetModelImpl.ENTITY_CACHE_ENABLED,
			ReTweetModelImpl.FINDER_CACHE_ENABLED, ReTweetImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ReTweetModelImpl.ENTITY_CACHE_ENABLED,
			ReTweetModelImpl.FINDER_CACHE_ENABLED, ReTweetImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ReTweetModelImpl.ENTITY_CACHE_ENABLED,
			ReTweetModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_TWEETID = new FinderPath(ReTweetModelImpl.ENTITY_CACHE_ENABLED,
			ReTweetModelImpl.FINDER_CACHE_ENABLED, ReTweetImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByTweetId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TWEETID =
		new FinderPath(ReTweetModelImpl.ENTITY_CACHE_ENABLED,
			ReTweetModelImpl.FINDER_CACHE_ENABLED, ReTweetImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByTweetId",
			new String[] { Long.class.getName() },
			ReTweetModelImpl.TWEETID_COLUMN_BITMASK |
			ReTweetModelImpl.RETWEETDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_TWEETID = new FinderPath(ReTweetModelImpl.ENTITY_CACHE_ENABLED,
			ReTweetModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByTweetId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the re tweets where tweetId = &#63;.
	 *
	 * @param tweetId the tweet ID
	 * @return the matching re tweets
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ReTweet> findByTweetId(long tweetId) throws SystemException {
		return findByTweetId(tweetId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the re tweets where tweetId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.test.model.impl.ReTweetModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param tweetId the tweet ID
	 * @param start the lower bound of the range of re tweets
	 * @param end the upper bound of the range of re tweets (not inclusive)
	 * @return the range of matching re tweets
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ReTweet> findByTweetId(long tweetId, int start, int end)
		throws SystemException {
		return findByTweetId(tweetId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the re tweets where tweetId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.test.model.impl.ReTweetModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param tweetId the tweet ID
	 * @param start the lower bound of the range of re tweets
	 * @param end the upper bound of the range of re tweets (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching re tweets
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ReTweet> findByTweetId(long tweetId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TWEETID;
			finderArgs = new Object[] { tweetId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_TWEETID;
			finderArgs = new Object[] { tweetId, start, end, orderByComparator };
		}

		List<ReTweet> list = (List<ReTweet>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ReTweet reTweet : list) {
				if ((tweetId != reTweet.getTweetId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_RETWEET_WHERE);

			query.append(_FINDER_COLUMN_TWEETID_TWEETID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ReTweetModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(tweetId);

				if (!pagination) {
					list = (List<ReTweet>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ReTweet>(list);
				}
				else {
					list = (List<ReTweet>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first re tweet in the ordered set where tweetId = &#63;.
	 *
	 * @param tweetId the tweet ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching re tweet
	 * @throws com.test.NoSuchReTweetException if a matching re tweet could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ReTweet findByTweetId_First(long tweetId,
		OrderByComparator orderByComparator)
		throws NoSuchReTweetException, SystemException {
		ReTweet reTweet = fetchByTweetId_First(tweetId, orderByComparator);

		if (reTweet != null) {
			return reTweet;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("tweetId=");
		msg.append(tweetId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchReTweetException(msg.toString());
	}

	/**
	 * Returns the first re tweet in the ordered set where tweetId = &#63;.
	 *
	 * @param tweetId the tweet ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching re tweet, or <code>null</code> if a matching re tweet could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ReTweet fetchByTweetId_First(long tweetId,
		OrderByComparator orderByComparator) throws SystemException {
		List<ReTweet> list = findByTweetId(tweetId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last re tweet in the ordered set where tweetId = &#63;.
	 *
	 * @param tweetId the tweet ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching re tweet
	 * @throws com.test.NoSuchReTweetException if a matching re tweet could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ReTweet findByTweetId_Last(long tweetId,
		OrderByComparator orderByComparator)
		throws NoSuchReTweetException, SystemException {
		ReTweet reTweet = fetchByTweetId_Last(tweetId, orderByComparator);

		if (reTweet != null) {
			return reTweet;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("tweetId=");
		msg.append(tweetId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchReTweetException(msg.toString());
	}

	/**
	 * Returns the last re tweet in the ordered set where tweetId = &#63;.
	 *
	 * @param tweetId the tweet ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching re tweet, or <code>null</code> if a matching re tweet could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ReTweet fetchByTweetId_Last(long tweetId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByTweetId(tweetId);

		if (count == 0) {
			return null;
		}

		List<ReTweet> list = findByTweetId(tweetId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the re tweets before and after the current re tweet in the ordered set where tweetId = &#63;.
	 *
	 * @param retweetId the primary key of the current re tweet
	 * @param tweetId the tweet ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next re tweet
	 * @throws com.test.NoSuchReTweetException if a re tweet with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ReTweet[] findByTweetId_PrevAndNext(long retweetId, long tweetId,
		OrderByComparator orderByComparator)
		throws NoSuchReTweetException, SystemException {
		ReTweet reTweet = findByPrimaryKey(retweetId);

		Session session = null;

		try {
			session = openSession();

			ReTweet[] array = new ReTweetImpl[3];

			array[0] = getByTweetId_PrevAndNext(session, reTweet, tweetId,
					orderByComparator, true);

			array[1] = reTweet;

			array[2] = getByTweetId_PrevAndNext(session, reTweet, tweetId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ReTweet getByTweetId_PrevAndNext(Session session,
		ReTweet reTweet, long tweetId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_RETWEET_WHERE);

		query.append(_FINDER_COLUMN_TWEETID_TWEETID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ReTweetModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(tweetId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(reTweet);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ReTweet> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the re tweets where tweetId = &#63; from the database.
	 *
	 * @param tweetId the tweet ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByTweetId(long tweetId) throws SystemException {
		for (ReTweet reTweet : findByTweetId(tweetId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(reTweet);
		}
	}

	/**
	 * Returns the number of re tweets where tweetId = &#63;.
	 *
	 * @param tweetId the tweet ID
	 * @return the number of matching re tweets
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByTweetId(long tweetId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_TWEETID;

		Object[] finderArgs = new Object[] { tweetId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_RETWEET_WHERE);

			query.append(_FINDER_COLUMN_TWEETID_TWEETID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(tweetId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_TWEETID_TWEETID_2 = "reTweet.tweetId = ?";

	public ReTweetPersistenceImpl() {
		setModelClass(ReTweet.class);
	}

	/**
	 * Caches the re tweet in the entity cache if it is enabled.
	 *
	 * @param reTweet the re tweet
	 */
	@Override
	public void cacheResult(ReTweet reTweet) {
		EntityCacheUtil.putResult(ReTweetModelImpl.ENTITY_CACHE_ENABLED,
			ReTweetImpl.class, reTweet.getPrimaryKey(), reTweet);

		reTweet.resetOriginalValues();
	}

	/**
	 * Caches the re tweets in the entity cache if it is enabled.
	 *
	 * @param reTweets the re tweets
	 */
	@Override
	public void cacheResult(List<ReTweet> reTweets) {
		for (ReTweet reTweet : reTweets) {
			if (EntityCacheUtil.getResult(
						ReTweetModelImpl.ENTITY_CACHE_ENABLED,
						ReTweetImpl.class, reTweet.getPrimaryKey()) == null) {
				cacheResult(reTweet);
			}
			else {
				reTweet.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all re tweets.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ReTweetImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ReTweetImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the re tweet.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ReTweet reTweet) {
		EntityCacheUtil.removeResult(ReTweetModelImpl.ENTITY_CACHE_ENABLED,
			ReTweetImpl.class, reTweet.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<ReTweet> reTweets) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ReTweet reTweet : reTweets) {
			EntityCacheUtil.removeResult(ReTweetModelImpl.ENTITY_CACHE_ENABLED,
				ReTweetImpl.class, reTweet.getPrimaryKey());
		}
	}

	/**
	 * Creates a new re tweet with the primary key. Does not add the re tweet to the database.
	 *
	 * @param retweetId the primary key for the new re tweet
	 * @return the new re tweet
	 */
	@Override
	public ReTweet create(long retweetId) {
		ReTweet reTweet = new ReTweetImpl();

		reTweet.setNew(true);
		reTweet.setPrimaryKey(retweetId);

		return reTweet;
	}

	/**
	 * Removes the re tweet with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param retweetId the primary key of the re tweet
	 * @return the re tweet that was removed
	 * @throws com.test.NoSuchReTweetException if a re tweet with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ReTweet remove(long retweetId)
		throws NoSuchReTweetException, SystemException {
		return remove((Serializable)retweetId);
	}

	/**
	 * Removes the re tweet with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the re tweet
	 * @return the re tweet that was removed
	 * @throws com.test.NoSuchReTweetException if a re tweet with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ReTweet remove(Serializable primaryKey)
		throws NoSuchReTweetException, SystemException {
		Session session = null;

		try {
			session = openSession();

			ReTweet reTweet = (ReTweet)session.get(ReTweetImpl.class, primaryKey);

			if (reTweet == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchReTweetException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(reTweet);
		}
		catch (NoSuchReTweetException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ReTweet removeImpl(ReTweet reTweet) throws SystemException {
		reTweet = toUnwrappedModel(reTweet);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(reTweet)) {
				reTweet = (ReTweet)session.get(ReTweetImpl.class,
						reTweet.getPrimaryKeyObj());
			}

			if (reTweet != null) {
				session.delete(reTweet);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (reTweet != null) {
			clearCache(reTweet);
		}

		return reTweet;
	}

	@Override
	public ReTweet updateImpl(com.test.model.ReTweet reTweet)
		throws SystemException {
		reTweet = toUnwrappedModel(reTweet);

		boolean isNew = reTweet.isNew();

		ReTweetModelImpl reTweetModelImpl = (ReTweetModelImpl)reTweet;

		Session session = null;

		try {
			session = openSession();

			if (reTweet.isNew()) {
				session.save(reTweet);

				reTweet.setNew(false);
			}
			else {
				session.merge(reTweet);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ReTweetModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((reTweetModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TWEETID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						reTweetModelImpl.getOriginalTweetId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TWEETID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TWEETID,
					args);

				args = new Object[] { reTweetModelImpl.getTweetId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TWEETID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TWEETID,
					args);
			}
		}

		EntityCacheUtil.putResult(ReTweetModelImpl.ENTITY_CACHE_ENABLED,
			ReTweetImpl.class, reTweet.getPrimaryKey(), reTweet);

		return reTweet;
	}

	protected ReTweet toUnwrappedModel(ReTweet reTweet) {
		if (reTweet instanceof ReTweetImpl) {
			return reTweet;
		}

		ReTweetImpl reTweetImpl = new ReTweetImpl();

		reTweetImpl.setNew(reTweet.isNew());
		reTweetImpl.setPrimaryKey(reTweet.getPrimaryKey());

		reTweetImpl.setRetweetId(reTweet.getRetweetId());
		reTweetImpl.setTweetId(reTweet.getTweetId());
		reTweetImpl.setRetweet(reTweet.getRetweet());
		reTweetImpl.setIcon(reTweet.getIcon());
		reTweetImpl.setRetweetDate(reTweet.getRetweetDate());
		reTweetImpl.setCreateDate(reTweet.getCreateDate());
		reTweetImpl.setModifiedDate(reTweet.getModifiedDate());

		return reTweetImpl;
	}

	/**
	 * Returns the re tweet with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the re tweet
	 * @return the re tweet
	 * @throws com.test.NoSuchReTweetException if a re tweet with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ReTweet findByPrimaryKey(Serializable primaryKey)
		throws NoSuchReTweetException, SystemException {
		ReTweet reTweet = fetchByPrimaryKey(primaryKey);

		if (reTweet == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchReTweetException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return reTweet;
	}

	/**
	 * Returns the re tweet with the primary key or throws a {@link com.test.NoSuchReTweetException} if it could not be found.
	 *
	 * @param retweetId the primary key of the re tweet
	 * @return the re tweet
	 * @throws com.test.NoSuchReTweetException if a re tweet with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ReTweet findByPrimaryKey(long retweetId)
		throws NoSuchReTweetException, SystemException {
		return findByPrimaryKey((Serializable)retweetId);
	}

	/**
	 * Returns the re tweet with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the re tweet
	 * @return the re tweet, or <code>null</code> if a re tweet with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ReTweet fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		ReTweet reTweet = (ReTweet)EntityCacheUtil.getResult(ReTweetModelImpl.ENTITY_CACHE_ENABLED,
				ReTweetImpl.class, primaryKey);

		if (reTweet == _nullReTweet) {
			return null;
		}

		if (reTweet == null) {
			Session session = null;

			try {
				session = openSession();

				reTweet = (ReTweet)session.get(ReTweetImpl.class, primaryKey);

				if (reTweet != null) {
					cacheResult(reTweet);
				}
				else {
					EntityCacheUtil.putResult(ReTweetModelImpl.ENTITY_CACHE_ENABLED,
						ReTweetImpl.class, primaryKey, _nullReTweet);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(ReTweetModelImpl.ENTITY_CACHE_ENABLED,
					ReTweetImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return reTweet;
	}

	/**
	 * Returns the re tweet with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param retweetId the primary key of the re tweet
	 * @return the re tweet, or <code>null</code> if a re tweet with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ReTweet fetchByPrimaryKey(long retweetId) throws SystemException {
		return fetchByPrimaryKey((Serializable)retweetId);
	}

	/**
	 * Returns all the re tweets.
	 *
	 * @return the re tweets
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ReTweet> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the re tweets.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.test.model.impl.ReTweetModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of re tweets
	 * @param end the upper bound of the range of re tweets (not inclusive)
	 * @return the range of re tweets
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ReTweet> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the re tweets.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.test.model.impl.ReTweetModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of re tweets
	 * @param end the upper bound of the range of re tweets (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of re tweets
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ReTweet> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ReTweet> list = (List<ReTweet>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_RETWEET);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_RETWEET;

				if (pagination) {
					sql = sql.concat(ReTweetModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ReTweet>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ReTweet>(list);
				}
				else {
					list = (List<ReTweet>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the re tweets from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (ReTweet reTweet : findAll()) {
			remove(reTweet);
		}
	}

	/**
	 * Returns the number of re tweets.
	 *
	 * @return the number of re tweets
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_RETWEET);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the re tweet persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.test.model.ReTweet")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<ReTweet>> listenersList = new ArrayList<ModelListener<ReTweet>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<ReTweet>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ReTweetImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_RETWEET = "SELECT reTweet FROM ReTweet reTweet";
	private static final String _SQL_SELECT_RETWEET_WHERE = "SELECT reTweet FROM ReTweet reTweet WHERE ";
	private static final String _SQL_COUNT_RETWEET = "SELECT COUNT(reTweet) FROM ReTweet reTweet";
	private static final String _SQL_COUNT_RETWEET_WHERE = "SELECT COUNT(reTweet) FROM ReTweet reTweet WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "reTweet.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ReTweet exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No ReTweet exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ReTweetPersistenceImpl.class);
	private static ReTweet _nullReTweet = new ReTweetImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<ReTweet> toCacheModel() {
				return _nullReTweetCacheModel;
			}
		};

	private static CacheModel<ReTweet> _nullReTweetCacheModel = new CacheModel<ReTweet>() {
			@Override
			public ReTweet toEntityModel() {
				return _nullReTweet;
			}
		};
}