/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.test.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import com.test.NoSuchretweetArcException;

import com.test.model.impl.retweetArcImpl;
import com.test.model.impl.retweetArcModelImpl;
import com.test.model.retweetArc;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the retweet arc service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author takuro
 * @see retweetArcPersistence
 * @see retweetArcUtil
 * @generated
 */
public class retweetArcPersistenceImpl extends BasePersistenceImpl<retweetArc>
	implements retweetArcPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link retweetArcUtil} to access the retweet arc persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = retweetArcImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(retweetArcModelImpl.ENTITY_CACHE_ENABLED,
			retweetArcModelImpl.FINDER_CACHE_ENABLED, retweetArcImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(retweetArcModelImpl.ENTITY_CACHE_ENABLED,
			retweetArcModelImpl.FINDER_CACHE_ENABLED, retweetArcImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(retweetArcModelImpl.ENTITY_CACHE_ENABLED,
			retweetArcModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_KEYWORD = new FinderPath(retweetArcModelImpl.ENTITY_CACHE_ENABLED,
			retweetArcModelImpl.FINDER_CACHE_ENABLED, retweetArcImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByKeyword",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_KEYWORD =
		new FinderPath(retweetArcModelImpl.ENTITY_CACHE_ENABLED,
			retweetArcModelImpl.FINDER_CACHE_ENABLED, retweetArcImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByKeyword",
			new String[] { String.class.getName() },
			retweetArcModelImpl.KEYWORD_COLUMN_BITMASK |
			retweetArcModelImpl.RETWEETDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_KEYWORD = new FinderPath(retweetArcModelImpl.ENTITY_CACHE_ENABLED,
			retweetArcModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByKeyword",
			new String[] { String.class.getName() });

	/**
	 * Returns all the retweet arcs where keyword = &#63;.
	 *
	 * @param keyword the keyword
	 * @return the matching retweet arcs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<retweetArc> findByKeyword(String keyword)
		throws SystemException {
		return findByKeyword(keyword, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the retweet arcs where keyword = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.test.model.impl.retweetArcModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param keyword the keyword
	 * @param start the lower bound of the range of retweet arcs
	 * @param end the upper bound of the range of retweet arcs (not inclusive)
	 * @return the range of matching retweet arcs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<retweetArc> findByKeyword(String keyword, int start, int end)
		throws SystemException {
		return findByKeyword(keyword, start, end, null);
	}

	/**
	 * Returns an ordered range of all the retweet arcs where keyword = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.test.model.impl.retweetArcModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param keyword the keyword
	 * @param start the lower bound of the range of retweet arcs
	 * @param end the upper bound of the range of retweet arcs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching retweet arcs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<retweetArc> findByKeyword(String keyword, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_KEYWORD;
			finderArgs = new Object[] { keyword };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_KEYWORD;
			finderArgs = new Object[] { keyword, start, end, orderByComparator };
		}

		List<retweetArc> list = (List<retweetArc>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (retweetArc retweetArc : list) {
				if (!Validator.equals(keyword, retweetArc.getKeyword())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_RETWEETARC_WHERE);

			boolean bindKeyword = false;

			if (keyword == null) {
				query.append(_FINDER_COLUMN_KEYWORD_KEYWORD_1);
			}
			else if (keyword.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_KEYWORD_KEYWORD_3);
			}
			else {
				bindKeyword = true;

				query.append(_FINDER_COLUMN_KEYWORD_KEYWORD_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(retweetArcModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindKeyword) {
					qPos.add(keyword);
				}

				if (!pagination) {
					list = (List<retweetArc>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<retweetArc>(list);
				}
				else {
					list = (List<retweetArc>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first retweet arc in the ordered set where keyword = &#63;.
	 *
	 * @param keyword the keyword
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching retweet arc
	 * @throws com.test.NoSuchretweetArcException if a matching retweet arc could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public retweetArc findByKeyword_First(String keyword,
		OrderByComparator orderByComparator)
		throws NoSuchretweetArcException, SystemException {
		retweetArc retweetArc = fetchByKeyword_First(keyword, orderByComparator);

		if (retweetArc != null) {
			return retweetArc;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("keyword=");
		msg.append(keyword);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchretweetArcException(msg.toString());
	}

	/**
	 * Returns the first retweet arc in the ordered set where keyword = &#63;.
	 *
	 * @param keyword the keyword
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching retweet arc, or <code>null</code> if a matching retweet arc could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public retweetArc fetchByKeyword_First(String keyword,
		OrderByComparator orderByComparator) throws SystemException {
		List<retweetArc> list = findByKeyword(keyword, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last retweet arc in the ordered set where keyword = &#63;.
	 *
	 * @param keyword the keyword
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching retweet arc
	 * @throws com.test.NoSuchretweetArcException if a matching retweet arc could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public retweetArc findByKeyword_Last(String keyword,
		OrderByComparator orderByComparator)
		throws NoSuchretweetArcException, SystemException {
		retweetArc retweetArc = fetchByKeyword_Last(keyword, orderByComparator);

		if (retweetArc != null) {
			return retweetArc;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("keyword=");
		msg.append(keyword);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchretweetArcException(msg.toString());
	}

	/**
	 * Returns the last retweet arc in the ordered set where keyword = &#63;.
	 *
	 * @param keyword the keyword
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching retweet arc, or <code>null</code> if a matching retweet arc could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public retweetArc fetchByKeyword_Last(String keyword,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByKeyword(keyword);

		if (count == 0) {
			return null;
		}

		List<retweetArc> list = findByKeyword(keyword, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the retweet arcs before and after the current retweet arc in the ordered set where keyword = &#63;.
	 *
	 * @param retweetArcPK the primary key of the current retweet arc
	 * @param keyword the keyword
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next retweet arc
	 * @throws com.test.NoSuchretweetArcException if a retweet arc with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public retweetArc[] findByKeyword_PrevAndNext(retweetArcPK retweetArcPK,
		String keyword, OrderByComparator orderByComparator)
		throws NoSuchretweetArcException, SystemException {
		retweetArc retweetArc = findByPrimaryKey(retweetArcPK);

		Session session = null;

		try {
			session = openSession();

			retweetArc[] array = new retweetArcImpl[3];

			array[0] = getByKeyword_PrevAndNext(session, retweetArc, keyword,
					orderByComparator, true);

			array[1] = retweetArc;

			array[2] = getByKeyword_PrevAndNext(session, retweetArc, keyword,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected retweetArc getByKeyword_PrevAndNext(Session session,
		retweetArc retweetArc, String keyword,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_RETWEETARC_WHERE);

		boolean bindKeyword = false;

		if (keyword == null) {
			query.append(_FINDER_COLUMN_KEYWORD_KEYWORD_1);
		}
		else if (keyword.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_KEYWORD_KEYWORD_3);
		}
		else {
			bindKeyword = true;

			query.append(_FINDER_COLUMN_KEYWORD_KEYWORD_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(retweetArcModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindKeyword) {
			qPos.add(keyword);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(retweetArc);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<retweetArc> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the retweet arcs where keyword = &#63; from the database.
	 *
	 * @param keyword the keyword
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByKeyword(String keyword) throws SystemException {
		for (retweetArc retweetArc : findByKeyword(keyword, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(retweetArc);
		}
	}

	/**
	 * Returns the number of retweet arcs where keyword = &#63;.
	 *
	 * @param keyword the keyword
	 * @return the number of matching retweet arcs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByKeyword(String keyword) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_KEYWORD;

		Object[] finderArgs = new Object[] { keyword };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_RETWEETARC_WHERE);

			boolean bindKeyword = false;

			if (keyword == null) {
				query.append(_FINDER_COLUMN_KEYWORD_KEYWORD_1);
			}
			else if (keyword.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_KEYWORD_KEYWORD_3);
			}
			else {
				bindKeyword = true;

				query.append(_FINDER_COLUMN_KEYWORD_KEYWORD_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindKeyword) {
					qPos.add(keyword);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_KEYWORD_KEYWORD_1 = "retweetArc.id.keyword IS NULL";
	private static final String _FINDER_COLUMN_KEYWORD_KEYWORD_2 = "retweetArc.id.keyword = ?";
	private static final String _FINDER_COLUMN_KEYWORD_KEYWORD_3 = "(retweetArc.id.keyword IS NULL OR retweetArc.id.keyword = '')";

	public retweetArcPersistenceImpl() {
		setModelClass(retweetArc.class);
	}

	/**
	 * Caches the retweet arc in the entity cache if it is enabled.
	 *
	 * @param retweetArc the retweet arc
	 */
	@Override
	public void cacheResult(retweetArc retweetArc) {
		EntityCacheUtil.putResult(retweetArcModelImpl.ENTITY_CACHE_ENABLED,
			retweetArcImpl.class, retweetArc.getPrimaryKey(), retweetArc);

		retweetArc.resetOriginalValues();
	}

	/**
	 * Caches the retweet arcs in the entity cache if it is enabled.
	 *
	 * @param retweetArcs the retweet arcs
	 */
	@Override
	public void cacheResult(List<retweetArc> retweetArcs) {
		for (retweetArc retweetArc : retweetArcs) {
			if (EntityCacheUtil.getResult(
						retweetArcModelImpl.ENTITY_CACHE_ENABLED,
						retweetArcImpl.class, retweetArc.getPrimaryKey()) == null) {
				cacheResult(retweetArc);
			}
			else {
				retweetArc.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all retweet arcs.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(retweetArcImpl.class.getName());
		}

		EntityCacheUtil.clearCache(retweetArcImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the retweet arc.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(retweetArc retweetArc) {
		EntityCacheUtil.removeResult(retweetArcModelImpl.ENTITY_CACHE_ENABLED,
			retweetArcImpl.class, retweetArc.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<retweetArc> retweetArcs) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (retweetArc retweetArc : retweetArcs) {
			EntityCacheUtil.removeResult(retweetArcModelImpl.ENTITY_CACHE_ENABLED,
				retweetArcImpl.class, retweetArc.getPrimaryKey());
		}
	}

	/**
	 * Creates a new retweet arc with the primary key. Does not add the retweet arc to the database.
	 *
	 * @param retweetArcPK the primary key for the new retweet arc
	 * @return the new retweet arc
	 */
	@Override
	public retweetArc create(retweetArcPK retweetArcPK) {
		retweetArc retweetArc = new retweetArcImpl();

		retweetArc.setNew(true);
		retweetArc.setPrimaryKey(retweetArcPK);

		return retweetArc;
	}

	/**
	 * Removes the retweet arc with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param retweetArcPK the primary key of the retweet arc
	 * @return the retweet arc that was removed
	 * @throws com.test.NoSuchretweetArcException if a retweet arc with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public retweetArc remove(retweetArcPK retweetArcPK)
		throws NoSuchretweetArcException, SystemException {
		return remove((Serializable)retweetArcPK);
	}

	/**
	 * Removes the retweet arc with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the retweet arc
	 * @return the retweet arc that was removed
	 * @throws com.test.NoSuchretweetArcException if a retweet arc with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public retweetArc remove(Serializable primaryKey)
		throws NoSuchretweetArcException, SystemException {
		Session session = null;

		try {
			session = openSession();

			retweetArc retweetArc = (retweetArc)session.get(retweetArcImpl.class,
					primaryKey);

			if (retweetArc == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchretweetArcException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(retweetArc);
		}
		catch (NoSuchretweetArcException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected retweetArc removeImpl(retweetArc retweetArc)
		throws SystemException {
		retweetArc = toUnwrappedModel(retweetArc);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(retweetArc)) {
				retweetArc = (retweetArc)session.get(retweetArcImpl.class,
						retweetArc.getPrimaryKeyObj());
			}

			if (retweetArc != null) {
				session.delete(retweetArc);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (retweetArc != null) {
			clearCache(retweetArc);
		}

		return retweetArc;
	}

	@Override
	public retweetArc updateImpl(com.test.model.retweetArc retweetArc)
		throws SystemException {
		retweetArc = toUnwrappedModel(retweetArc);

		boolean isNew = retweetArc.isNew();

		retweetArcModelImpl retweetArcModelImpl = (retweetArcModelImpl)retweetArc;

		Session session = null;

		try {
			session = openSession();

			if (retweetArc.isNew()) {
				session.save(retweetArc);

				retweetArc.setNew(false);
			}
			else {
				session.merge(retweetArc);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !retweetArcModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((retweetArcModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_KEYWORD.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						retweetArcModelImpl.getOriginalKeyword()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_KEYWORD, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_KEYWORD,
					args);

				args = new Object[] { retweetArcModelImpl.getKeyword() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_KEYWORD, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_KEYWORD,
					args);
			}
		}

		EntityCacheUtil.putResult(retweetArcModelImpl.ENTITY_CACHE_ENABLED,
			retweetArcImpl.class, retweetArc.getPrimaryKey(), retweetArc);

		return retweetArc;
	}

	protected retweetArc toUnwrappedModel(retweetArc retweetArc) {
		if (retweetArc instanceof retweetArcImpl) {
			return retweetArc;
		}

		retweetArcImpl retweetArcImpl = new retweetArcImpl();

		retweetArcImpl.setNew(retweetArc.isNew());
		retweetArcImpl.setPrimaryKey(retweetArc.getPrimaryKey());

		retweetArcImpl.setRetweetId(retweetArc.getRetweetId());
		retweetArcImpl.setKeyword(retweetArc.getKeyword());
		retweetArcImpl.setRetweet(retweetArc.getRetweet());
		retweetArcImpl.setIcon(retweetArc.getIcon());
		retweetArcImpl.setRetweetDate(retweetArc.getRetweetDate());
		retweetArcImpl.setCreateDate(retweetArc.getCreateDate());
		retweetArcImpl.setModifiedDate(retweetArc.getModifiedDate());

		return retweetArcImpl;
	}

	/**
	 * Returns the retweet arc with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the retweet arc
	 * @return the retweet arc
	 * @throws com.test.NoSuchretweetArcException if a retweet arc with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public retweetArc findByPrimaryKey(Serializable primaryKey)
		throws NoSuchretweetArcException, SystemException {
		retweetArc retweetArc = fetchByPrimaryKey(primaryKey);

		if (retweetArc == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchretweetArcException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return retweetArc;
	}

	/**
	 * Returns the retweet arc with the primary key or throws a {@link com.test.NoSuchretweetArcException} if it could not be found.
	 *
	 * @param retweetArcPK the primary key of the retweet arc
	 * @return the retweet arc
	 * @throws com.test.NoSuchretweetArcException if a retweet arc with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public retweetArc findByPrimaryKey(retweetArcPK retweetArcPK)
		throws NoSuchretweetArcException, SystemException {
		return findByPrimaryKey((Serializable)retweetArcPK);
	}

	/**
	 * Returns the retweet arc with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the retweet arc
	 * @return the retweet arc, or <code>null</code> if a retweet arc with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public retweetArc fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		retweetArc retweetArc = (retweetArc)EntityCacheUtil.getResult(retweetArcModelImpl.ENTITY_CACHE_ENABLED,
				retweetArcImpl.class, primaryKey);

		if (retweetArc == _nullretweetArc) {
			return null;
		}

		if (retweetArc == null) {
			Session session = null;

			try {
				session = openSession();

				retweetArc = (retweetArc)session.get(retweetArcImpl.class,
						primaryKey);

				if (retweetArc != null) {
					cacheResult(retweetArc);
				}
				else {
					EntityCacheUtil.putResult(retweetArcModelImpl.ENTITY_CACHE_ENABLED,
						retweetArcImpl.class, primaryKey, _nullretweetArc);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(retweetArcModelImpl.ENTITY_CACHE_ENABLED,
					retweetArcImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return retweetArc;
	}

	/**
	 * Returns the retweet arc with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param retweetArcPK the primary key of the retweet arc
	 * @return the retweet arc, or <code>null</code> if a retweet arc with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public retweetArc fetchByPrimaryKey(retweetArcPK retweetArcPK)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)retweetArcPK);
	}

	/**
	 * Returns all the retweet arcs.
	 *
	 * @return the retweet arcs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<retweetArc> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the retweet arcs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.test.model.impl.retweetArcModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of retweet arcs
	 * @param end the upper bound of the range of retweet arcs (not inclusive)
	 * @return the range of retweet arcs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<retweetArc> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the retweet arcs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.test.model.impl.retweetArcModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of retweet arcs
	 * @param end the upper bound of the range of retweet arcs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of retweet arcs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<retweetArc> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<retweetArc> list = (List<retweetArc>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_RETWEETARC);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_RETWEETARC;

				if (pagination) {
					sql = sql.concat(retweetArcModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<retweetArc>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<retweetArc>(list);
				}
				else {
					list = (List<retweetArc>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the retweet arcs from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (retweetArc retweetArc : findAll()) {
			remove(retweetArc);
		}
	}

	/**
	 * Returns the number of retweet arcs.
	 *
	 * @return the number of retweet arcs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_RETWEETARC);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the retweet arc persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.test.model.retweetArc")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<retweetArc>> listenersList = new ArrayList<ModelListener<retweetArc>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<retweetArc>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(retweetArcImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_RETWEETARC = "SELECT retweetArc FROM retweetArc retweetArc";
	private static final String _SQL_SELECT_RETWEETARC_WHERE = "SELECT retweetArc FROM retweetArc retweetArc WHERE ";
	private static final String _SQL_COUNT_RETWEETARC = "SELECT COUNT(retweetArc) FROM retweetArc retweetArc";
	private static final String _SQL_COUNT_RETWEETARC_WHERE = "SELECT COUNT(retweetArc) FROM retweetArc retweetArc WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "retweetArc.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No retweetArc exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No retweetArc exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(retweetArcPersistenceImpl.class);
	private static retweetArc _nullretweetArc = new retweetArcImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<retweetArc> toCacheModel() {
				return _nullretweetArcCacheModel;
			}
		};

	private static CacheModel<retweetArc> _nullretweetArcCacheModel = new CacheModel<retweetArc>() {
			@Override
			public retweetArc toEntityModel() {
				return _nullretweetArc;
			}
		};
}