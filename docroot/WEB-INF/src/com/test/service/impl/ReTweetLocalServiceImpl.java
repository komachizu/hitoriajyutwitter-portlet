/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.test.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;
import com.test.model.ReTweet;
import com.test.model.Tweet;
import com.test.service.base.ReTweetLocalServiceBaseImpl;

/**
 * The implementation of the re tweet local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.test.service.ReTweetLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author takuro
 * @see com.test.service.base.ReTweetLocalServiceBaseImpl
 * @see com.test.service.ReTweetLocalServiceUtil
 */
public class ReTweetLocalServiceImpl extends ReTweetLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.test.service.ReTweetLocalServiceUtil} to access the re tweet local service.
	 */
	public List<ReTweet> getReTweetList(long tweetId){
		List<ReTweet> ret = null;
		try {
			ret = getReTweetPersistence().findByTweetId(tweetId);
		} catch (SystemException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}		
		return ret;
	}
}