/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.test.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;
import com.test.model.Tweet;
import com.test.service.TweetLocalServiceUtil;
import com.test.service.base.TweetServiceBaseImpl;

/**
 * The implementation of the tweet remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.test.service.TweetService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author takuro
 * @see com.test.service.base.TweetServiceBaseImpl
 * @see com.test.service.TweetServiceUtil
 */
public class TweetServiceImpl extends TweetServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.test.service.TweetServiceUtil} to access the tweet remote service.
	 */
	public List<Tweet> getTweetList(long userId){
		return TweetLocalServiceUtil.getTweetList(userId);
	}
	
	public long execTweet(long userId, String tweetVal, String keywordCSV) throws SystemException{
		return TweetLocalServiceUtil.execTweet(userId, tweetVal, keywordCSV);
	}
}