/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.test.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.RateLimitStatus;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.conf.ConfigurationBuilder;

import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.test.model.ReTweet;
import com.test.model.Tweet;
import com.test.model.retweetArc;
import com.test.service.ReTweetLocalServiceUtil;
import com.test.service.TweetLocalServiceUtil;
import com.test.service.retweetArcLocalServiceUtil;
import com.test.service.base.TweetLocalServiceBaseImpl;
import com.test.service.persistence.retweetArcPK;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
/**
 * The implementation of the tweet local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.test.service.TweetLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author takuro
 * @see com.test.service.base.TweetLocalServiceBaseImpl
 * @see com.test.service.TweetLocalServiceUtil
 */
public class TweetLocalServiceImpl extends TweetLocalServiceBaseImpl {


	private static Log _log = LogFactoryUtil.getLog(TweetLocalServiceImpl.class);
	

	private final static String CONSUMER_KEY =        "nJB3uWx8hyB5RTq1i3fuAKcp2";
	private final static String CONSUMER_SECRET =     "aObytIgQ8b4WKMvQ4KtURkhqH2SVPniXSvE0DjL4GqntzbFLvl";
	private final static String ACCESS_TOKEN =        "3301643772-F6offA8GFTDsGS9acC1XM7zUhyhwUPBbukz8Xaq";
	private final static String ACCESS_TOKEN_SECRET = "dQkZhdVkpKc00redP1GDRGuS4103teQyb90yUsvb5nyFL";
	
	private ConfigurationBuilder confbuilder;
	private TwitterFactory twitterfactory;
	private Twitter twitter;
	private AccessToken accessToken;

	public TweetLocalServiceImpl() {
		this.confbuilder = new ConfigurationBuilder();
		this.confbuilder.setDebugEnabled(true);
		this.confbuilder.setOAuthConsumerKey(CONSUMER_KEY);
		this.confbuilder.setOAuthConsumerSecret(CONSUMER_SECRET);
		this.confbuilder.setOAuthAccessToken(null);
		this.confbuilder.setOAuthAccessTokenSecret(null);
		
		this.twitterfactory = new TwitterFactory(confbuilder.build());
		this.twitter = twitterfactory.getInstance();
		this.accessToken = new AccessToken(ACCESS_TOKEN, ACCESS_TOKEN_SECRET);
        twitter.setOAuthAccessToken(accessToken);
	}
	
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.test.service.TweetLocalServiceUtil} to access the tweet local service.
	 */
	public List<Tweet> getTweetList(long userId){
		List<Tweet> ret = null;
		try {
			ret = getTweetPersistence().findByUserId(userId);
		} catch (SystemException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}		
		return ret;
	}
	
	public long execTweet(long userId, String tweetVal ,String keywordCSV) throws SystemException{
		_log.info("execTweet userId:" + userId);
		_log.info("execTweet tweetVal:" + tweetVal);
		_log.info("execTweet keywordCSV:" + keywordCSV);
		
		// 発言の登録
		Date now = new Date();
		long did = CounterLocalServiceUtil.increment(Tweet.class.getName());
		Tweet tweet = TweetLocalServiceUtil.createTweet(did);
		tweet.setTweet(tweetVal);
		tweet.setUserId(userId);
		tweet.setBotTweet(false);
		tweet.setCreateDate(now);
		tweet.setModifiedDate(now);
		
		Tweet addedTweet = TweetLocalServiceUtil.addTweet(tweet);
		
		// Retweetを探す
		List<Status> searchedRetweets = getSearchRetweet(keywordCSV,now);
		
		for(Status status:searchedRetweets){
			
			// Retweet追加
			if(ReTweetLocalServiceUtil.fetchReTweet(status.getId()) == null){
				ReTweet retweet = ReTweetLocalServiceUtil.createReTweet(status.getId());
				retweet.setRetweetId(status.getId());
				retweet.setTweetId(addedTweet.getTweetId());
				retweet.setRetweet(status.getText());
				retweet.setIcon(status.getUser().getProfileImageURL());
				retweet.setRetweetDate(status.getCreatedAt());
				retweet.setCreateDate(now);
				retweet.setModifiedDate(now);
				
				ReTweetLocalServiceUtil.addReTweet(retweet);
			}
		}
		return addedTweet.getTweetId();
	}
	
	private List<Status> getSearchRetweet(String keywordCSV,Date now) throws SystemException{
        List<Status> retweetStatusList = new ArrayList<Status>(); 
        String[] keywords = keywordCSV.split(",");

        Status ret = null;

	        for(String key:keywords){
				ret = getStatusRetweet(key);
				
	        	if(ret != null){
	        		retweetStatusList.add(ret);
	        		
	        		retweetArcPK retweetArcPK =  new retweetArcPK(ret.getId(), key);
	        		if(retweetArcLocalServiceUtil.fetchretweetArc(retweetArcPK) == null){
	        			retweetArc ra = retweetArcLocalServiceUtil.createretweetArc(retweetArcPK);
	        			ra.setRetweet(ret.getText());
	        			ra.setIcon(ret.getUser().getProfileImageURL());
	        			ra.setRetweetDate(ret.getCreatedAt());
	        			ra.setCreateDate(now);
	        			ra.setModifiedDate(now);
	        			
	        			retweetArcLocalServiceUtil.addretweetArc(ra);
	        		}
	        		
	        	}
	        }

        return retweetStatusList;
	}
	
	@SuppressWarnings("unused")
	private Status getStatusRetweet(String keyword){
		_log.info("getStatusRetweet keyword:"+ keyword);
		Status retweetStatus = null;
		
        // 初期化・認証
//		ConfigurationBuilder confbuilder = new ConfigurationBuilder();
//		confbuilder.setDebugEnabled(true);
//		confbuilder.setOAuthConsumerKey(CONSUMER_KEY);
//		confbuilder.setOAuthConsumerSecret(CONSUMER_SECRET);
//		confbuilder.setOAuthAccessToken(null);
//		confbuilder.setOAuthAccessTokenSecret(null);
//		
//		TwitterFactory twitterfactory = new TwitterFactory(confbuilder.build());
//		Twitter twitter = twitterfactory.getInstance();
//		AccessToken accessToken = new AccessToken(ACCESS_TOKEN, ACCESS_TOKEN_SECRET);
//        twitter.setOAuthAccessToken(accessToken);
        
        
        Query query = new Query();
        query.setLang("ja");


        query.setCount(100);

        // 検索ワードをセット
        query.setQuery(keyword);
        
        // 検索実行
        QueryResult result;
		List<Status> statusList = new ArrayList<Status>();
        int retweetCount = 0;
		try {
	        
			result = this.twitter.search(query);

			if(result != null){
				statusList = result.getTweets();
			}else{
				return retweetStatus;
			}
			_log.info("ヒット数 : " + statusList.size());

	        for (Status status : statusList) {
	        	
	        	// リツイート除外
	        	if(status.getRetweetedStatus() != null){
	        		retweetCount++;
	        		continue;
	        	}
	        	
	        	// 利用制限オーバー
	        	if(isLimitOver()){
	        		return null;
	        	}
       	
		        // リツイート取得
	            retweetStatus = this.getRetweet(status.getId());
	            if(retweetStatus != null){
	            	_log.info("参照ツイート：" + status.getText());
	            	_log.info("参照ツイートのリツイート：" + retweetStatus.getText());
	            	return retweetStatus;
	            }
	        }      
		} catch (TwitterException e) {
			_log.error("利用制限オーバー getStatusRetweet");
		}
		return retweetStatus;
	}

	private boolean isLimitOver() throws TwitterException{
		Map<String,RateLimitStatus> map = this.twitter.getRateLimitStatus();
		RateLimitStatus rls = (RateLimitStatus)map.get("/statuses/retweets/:id");
		System.out.println(rls.getRemaining());
		
		if(rls.getRemaining() < 1){
			System.out.println("getRemaining:" + rls.getRemaining());
			System.out.println("getResetTimeInSeconds:" + rls.getResetTimeInSeconds());
			System.out.println("getSecondsUntilReset:" + rls.getSecondsUntilReset());
			
			_log.info("あと" + rls.getSecondsUntilReset() + "秒待ってください。");
			
			return true;
		}	
		return false;
	}
	
	private Status getRetweet(long tweetId){
		//Twitter twitter = new TwitterFactory().getInstance();
		try {
			
			List<Status> retweetList = this.twitter.getRetweets(tweetId);
			if(retweetList.size() > 0){	
				
				return (Status)retweetList.get(0);
			}
			
		} catch (TwitterException e) {
			_log.error("利用制限オーバー getRetweet");
		}
		return null;
	}
}