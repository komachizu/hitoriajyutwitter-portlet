/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.test.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.test.service.http.TweetServiceSoap}.
 *
 * @author takuro
 * @see com.test.service.http.TweetServiceSoap
 * @generated
 */
public class TweetSoap implements Serializable {
	public static TweetSoap toSoapModel(Tweet model) {
		TweetSoap soapModel = new TweetSoap();

		soapModel.setTweetId(model.getTweetId());
		soapModel.setTweet(model.getTweet());
		soapModel.setBotTweet(model.getBotTweet());
		soapModel.setUserId(model.getUserId());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());

		return soapModel;
	}

	public static TweetSoap[] toSoapModels(Tweet[] models) {
		TweetSoap[] soapModels = new TweetSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static TweetSoap[][] toSoapModels(Tweet[][] models) {
		TweetSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new TweetSoap[models.length][models[0].length];
		}
		else {
			soapModels = new TweetSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static TweetSoap[] toSoapModels(List<Tweet> models) {
		List<TweetSoap> soapModels = new ArrayList<TweetSoap>(models.size());

		for (Tweet model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new TweetSoap[soapModels.size()]);
	}

	public TweetSoap() {
	}

	public long getPrimaryKey() {
		return _tweetId;
	}

	public void setPrimaryKey(long pk) {
		setTweetId(pk);
	}

	public long getTweetId() {
		return _tweetId;
	}

	public void setTweetId(long tweetId) {
		_tweetId = tweetId;
	}

	public String getTweet() {
		return _tweet;
	}

	public void setTweet(String tweet) {
		_tweet = tweet;
	}

	public boolean getBotTweet() {
		return _botTweet;
	}

	public boolean isBotTweet() {
		return _botTweet;
	}

	public void setBotTweet(boolean botTweet) {
		_botTweet = botTweet;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	private long _tweetId;
	private String _tweet;
	private boolean _botTweet;
	private long _userId;
	private Date _createDate;
	private Date _modifiedDate;
}