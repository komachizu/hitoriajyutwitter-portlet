/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.test.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Tweet}.
 * </p>
 *
 * @author takuro
 * @see Tweet
 * @generated
 */
public class TweetWrapper implements Tweet, ModelWrapper<Tweet> {
	public TweetWrapper(Tweet tweet) {
		_tweet = tweet;
	}

	@Override
	public Class<?> getModelClass() {
		return Tweet.class;
	}

	@Override
	public String getModelClassName() {
		return Tweet.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("tweetId", getTweetId());
		attributes.put("tweet", getTweet());
		attributes.put("botTweet", getBotTweet());
		attributes.put("userId", getUserId());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long tweetId = (Long)attributes.get("tweetId");

		if (tweetId != null) {
			setTweetId(tweetId);
		}

		String tweet = (String)attributes.get("tweet");

		if (tweet != null) {
			setTweet(tweet);
		}

		Boolean botTweet = (Boolean)attributes.get("botTweet");

		if (botTweet != null) {
			setBotTweet(botTweet);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}
	}

	/**
	* Returns the primary key of this tweet.
	*
	* @return the primary key of this tweet
	*/
	@Override
	public long getPrimaryKey() {
		return _tweet.getPrimaryKey();
	}

	/**
	* Sets the primary key of this tweet.
	*
	* @param primaryKey the primary key of this tweet
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_tweet.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the tweet ID of this tweet.
	*
	* @return the tweet ID of this tweet
	*/
	@Override
	public long getTweetId() {
		return _tweet.getTweetId();
	}

	/**
	* Sets the tweet ID of this tweet.
	*
	* @param tweetId the tweet ID of this tweet
	*/
	@Override
	public void setTweetId(long tweetId) {
		_tweet.setTweetId(tweetId);
	}

	/**
	* Returns the tweet of this tweet.
	*
	* @return the tweet of this tweet
	*/
	@Override
	public java.lang.String getTweet() {
		return _tweet.getTweet();
	}

	/**
	* Sets the tweet of this tweet.
	*
	* @param tweet the tweet of this tweet
	*/
	@Override
	public void setTweet(java.lang.String tweet) {
		_tweet.setTweet(tweet);
	}

	/**
	* Returns the bot tweet of this tweet.
	*
	* @return the bot tweet of this tweet
	*/
	@Override
	public boolean getBotTweet() {
		return _tweet.getBotTweet();
	}

	/**
	* Returns <code>true</code> if this tweet is bot tweet.
	*
	* @return <code>true</code> if this tweet is bot tweet; <code>false</code> otherwise
	*/
	@Override
	public boolean isBotTweet() {
		return _tweet.isBotTweet();
	}

	/**
	* Sets whether this tweet is bot tweet.
	*
	* @param botTweet the bot tweet of this tweet
	*/
	@Override
	public void setBotTweet(boolean botTweet) {
		_tweet.setBotTweet(botTweet);
	}

	/**
	* Returns the user ID of this tweet.
	*
	* @return the user ID of this tweet
	*/
	@Override
	public long getUserId() {
		return _tweet.getUserId();
	}

	/**
	* Sets the user ID of this tweet.
	*
	* @param userId the user ID of this tweet
	*/
	@Override
	public void setUserId(long userId) {
		_tweet.setUserId(userId);
	}

	/**
	* Returns the user uuid of this tweet.
	*
	* @return the user uuid of this tweet
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _tweet.getUserUuid();
	}

	/**
	* Sets the user uuid of this tweet.
	*
	* @param userUuid the user uuid of this tweet
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_tweet.setUserUuid(userUuid);
	}

	/**
	* Returns the create date of this tweet.
	*
	* @return the create date of this tweet
	*/
	@Override
	public java.util.Date getCreateDate() {
		return _tweet.getCreateDate();
	}

	/**
	* Sets the create date of this tweet.
	*
	* @param createDate the create date of this tweet
	*/
	@Override
	public void setCreateDate(java.util.Date createDate) {
		_tweet.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this tweet.
	*
	* @return the modified date of this tweet
	*/
	@Override
	public java.util.Date getModifiedDate() {
		return _tweet.getModifiedDate();
	}

	/**
	* Sets the modified date of this tweet.
	*
	* @param modifiedDate the modified date of this tweet
	*/
	@Override
	public void setModifiedDate(java.util.Date modifiedDate) {
		_tweet.setModifiedDate(modifiedDate);
	}

	@Override
	public boolean isNew() {
		return _tweet.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_tweet.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _tweet.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_tweet.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _tweet.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _tweet.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_tweet.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _tweet.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_tweet.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_tweet.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_tweet.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new TweetWrapper((Tweet)_tweet.clone());
	}

	@Override
	public int compareTo(com.test.model.Tweet tweet) {
		return _tweet.compareTo(tweet);
	}

	@Override
	public int hashCode() {
		return _tweet.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.test.model.Tweet> toCacheModel() {
		return _tweet.toCacheModel();
	}

	@Override
	public com.test.model.Tweet toEscapedModel() {
		return new TweetWrapper(_tweet.toEscapedModel());
	}

	@Override
	public com.test.model.Tweet toUnescapedModel() {
		return new TweetWrapper(_tweet.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _tweet.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _tweet.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_tweet.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof TweetWrapper)) {
			return false;
		}

		TweetWrapper tweetWrapper = (TweetWrapper)obj;

		if (Validator.equals(_tweet, tweetWrapper._tweet)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public Tweet getWrappedTweet() {
		return _tweet;
	}

	@Override
	public Tweet getWrappedModel() {
		return _tweet;
	}

	@Override
	public void resetOriginalValues() {
		_tweet.resetOriginalValues();
	}

	private Tweet _tweet;
}