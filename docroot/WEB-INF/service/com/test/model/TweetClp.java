/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.test.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import com.test.service.ClpSerializer;
import com.test.service.TweetLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author takuro
 */
public class TweetClp extends BaseModelImpl<Tweet> implements Tweet {
	public TweetClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return Tweet.class;
	}

	@Override
	public String getModelClassName() {
		return Tweet.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _tweetId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setTweetId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _tweetId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("tweetId", getTweetId());
		attributes.put("tweet", getTweet());
		attributes.put("botTweet", getBotTweet());
		attributes.put("userId", getUserId());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long tweetId = (Long)attributes.get("tweetId");

		if (tweetId != null) {
			setTweetId(tweetId);
		}

		String tweet = (String)attributes.get("tweet");

		if (tweet != null) {
			setTweet(tweet);
		}

		Boolean botTweet = (Boolean)attributes.get("botTweet");

		if (botTweet != null) {
			setBotTweet(botTweet);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}
	}

	@Override
	public long getTweetId() {
		return _tweetId;
	}

	@Override
	public void setTweetId(long tweetId) {
		_tweetId = tweetId;

		if (_tweetRemoteModel != null) {
			try {
				Class<?> clazz = _tweetRemoteModel.getClass();

				Method method = clazz.getMethod("setTweetId", long.class);

				method.invoke(_tweetRemoteModel, tweetId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTweet() {
		return _tweet;
	}

	@Override
	public void setTweet(String tweet) {
		_tweet = tweet;

		if (_tweetRemoteModel != null) {
			try {
				Class<?> clazz = _tweetRemoteModel.getClass();

				Method method = clazz.getMethod("setTweet", String.class);

				method.invoke(_tweetRemoteModel, tweet);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getBotTweet() {
		return _botTweet;
	}

	@Override
	public boolean isBotTweet() {
		return _botTweet;
	}

	@Override
	public void setBotTweet(boolean botTweet) {
		_botTweet = botTweet;

		if (_tweetRemoteModel != null) {
			try {
				Class<?> clazz = _tweetRemoteModel.getClass();

				Method method = clazz.getMethod("setBotTweet", boolean.class);

				method.invoke(_tweetRemoteModel, botTweet);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		_userId = userId;

		if (_tweetRemoteModel != null) {
			try {
				Class<?> clazz = _tweetRemoteModel.getClass();

				Method method = clazz.getMethod("setUserId", long.class);

				method.invoke(_tweetRemoteModel, userId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	@Override
	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	@Override
	public Date getCreateDate() {
		return _createDate;
	}

	@Override
	public void setCreateDate(Date createDate) {
		_createDate = createDate;

		if (_tweetRemoteModel != null) {
			try {
				Class<?> clazz = _tweetRemoteModel.getClass();

				Method method = clazz.getMethod("setCreateDate", Date.class);

				method.invoke(_tweetRemoteModel, createDate);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getModifiedDate() {
		return _modifiedDate;
	}

	@Override
	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;

		if (_tweetRemoteModel != null) {
			try {
				Class<?> clazz = _tweetRemoteModel.getClass();

				Method method = clazz.getMethod("setModifiedDate", Date.class);

				method.invoke(_tweetRemoteModel, modifiedDate);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getTweetRemoteModel() {
		return _tweetRemoteModel;
	}

	public void setTweetRemoteModel(BaseModel<?> tweetRemoteModel) {
		_tweetRemoteModel = tweetRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _tweetRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_tweetRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			TweetLocalServiceUtil.addTweet(this);
		}
		else {
			TweetLocalServiceUtil.updateTweet(this);
		}
	}

	@Override
	public Tweet toEscapedModel() {
		return (Tweet)ProxyUtil.newProxyInstance(Tweet.class.getClassLoader(),
			new Class[] { Tweet.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		TweetClp clone = new TweetClp();

		clone.setTweetId(getTweetId());
		clone.setTweet(getTweet());
		clone.setBotTweet(getBotTweet());
		clone.setUserId(getUserId());
		clone.setCreateDate(getCreateDate());
		clone.setModifiedDate(getModifiedDate());

		return clone;
	}

	@Override
	public int compareTo(Tweet tweet) {
		int value = 0;

		value = DateUtil.compareTo(getCreateDate(), tweet.getCreateDate());

		value = value * -1;

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof TweetClp)) {
			return false;
		}

		TweetClp tweet = (TweetClp)obj;

		long primaryKey = tweet.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{tweetId=");
		sb.append(getTweetId());
		sb.append(", tweet=");
		sb.append(getTweet());
		sb.append(", botTweet=");
		sb.append(getBotTweet());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", createDate=");
		sb.append(getCreateDate());
		sb.append(", modifiedDate=");
		sb.append(getModifiedDate());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(22);

		sb.append("<model><model-name>");
		sb.append("com.test.model.Tweet");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>tweetId</column-name><column-value><![CDATA[");
		sb.append(getTweetId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tweet</column-name><column-value><![CDATA[");
		sb.append(getTweet());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>botTweet</column-name><column-value><![CDATA[");
		sb.append(getBotTweet());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>createDate</column-name><column-value><![CDATA[");
		sb.append(getCreateDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
		sb.append(getModifiedDate());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _tweetId;
	private String _tweet;
	private boolean _botTweet;
	private long _userId;
	private String _userUuid;
	private Date _createDate;
	private Date _modifiedDate;
	private BaseModel<?> _tweetRemoteModel;
}