/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.test.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;

import com.test.service.persistence.retweetArcPK;

import java.io.Serializable;

import java.util.Date;

/**
 * The base model interface for the retweetArc service. Represents a row in the &quot;hitoria_retweetArc&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link com.test.model.impl.retweetArcModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link com.test.model.impl.retweetArcImpl}.
 * </p>
 *
 * @author takuro
 * @see retweetArc
 * @see com.test.model.impl.retweetArcImpl
 * @see com.test.model.impl.retweetArcModelImpl
 * @generated
 */
public interface retweetArcModel extends BaseModel<retweetArc> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a retweet arc model instance should use the {@link retweetArc} interface instead.
	 */

	/**
	 * Returns the primary key of this retweet arc.
	 *
	 * @return the primary key of this retweet arc
	 */
	public retweetArcPK getPrimaryKey();

	/**
	 * Sets the primary key of this retweet arc.
	 *
	 * @param primaryKey the primary key of this retweet arc
	 */
	public void setPrimaryKey(retweetArcPK primaryKey);

	/**
	 * Returns the retweet ID of this retweet arc.
	 *
	 * @return the retweet ID of this retweet arc
	 */
	public long getRetweetId();

	/**
	 * Sets the retweet ID of this retweet arc.
	 *
	 * @param retweetId the retweet ID of this retweet arc
	 */
	public void setRetweetId(long retweetId);

	/**
	 * Returns the keyword of this retweet arc.
	 *
	 * @return the keyword of this retweet arc
	 */
	@AutoEscape
	public String getKeyword();

	/**
	 * Sets the keyword of this retweet arc.
	 *
	 * @param keyword the keyword of this retweet arc
	 */
	public void setKeyword(String keyword);

	/**
	 * Returns the retweet of this retweet arc.
	 *
	 * @return the retweet of this retweet arc
	 */
	@AutoEscape
	public String getRetweet();

	/**
	 * Sets the retweet of this retweet arc.
	 *
	 * @param retweet the retweet of this retweet arc
	 */
	public void setRetweet(String retweet);

	/**
	 * Returns the icon of this retweet arc.
	 *
	 * @return the icon of this retweet arc
	 */
	@AutoEscape
	public String getIcon();

	/**
	 * Sets the icon of this retweet arc.
	 *
	 * @param icon the icon of this retweet arc
	 */
	public void setIcon(String icon);

	/**
	 * Returns the retweet date of this retweet arc.
	 *
	 * @return the retweet date of this retweet arc
	 */
	public Date getRetweetDate();

	/**
	 * Sets the retweet date of this retweet arc.
	 *
	 * @param retweetDate the retweet date of this retweet arc
	 */
	public void setRetweetDate(Date retweetDate);

	/**
	 * Returns the create date of this retweet arc.
	 *
	 * @return the create date of this retweet arc
	 */
	public Date getCreateDate();

	/**
	 * Sets the create date of this retweet arc.
	 *
	 * @param createDate the create date of this retweet arc
	 */
	public void setCreateDate(Date createDate);

	/**
	 * Returns the modified date of this retweet arc.
	 *
	 * @return the modified date of this retweet arc
	 */
	public Date getModifiedDate();

	/**
	 * Sets the modified date of this retweet arc.
	 *
	 * @param modifiedDate the modified date of this retweet arc
	 */
	public void setModifiedDate(Date modifiedDate);

	@Override
	public boolean isNew();

	@Override
	public void setNew(boolean n);

	@Override
	public boolean isCachedModel();

	@Override
	public void setCachedModel(boolean cachedModel);

	@Override
	public boolean isEscapedModel();

	@Override
	public Serializable getPrimaryKeyObj();

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	@Override
	public ExpandoBridge getExpandoBridge();

	@Override
	public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	@Override
	public Object clone();

	@Override
	public int compareTo(retweetArc retweetArc);

	@Override
	public int hashCode();

	@Override
	public CacheModel<retweetArc> toCacheModel();

	@Override
	public retweetArc toEscapedModel();

	@Override
	public retweetArc toUnescapedModel();

	@Override
	public String toString();

	@Override
	public String toXmlString();
}