/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.test.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.test.service.http.ReTweetServiceSoap}.
 *
 * @author takuro
 * @see com.test.service.http.ReTweetServiceSoap
 * @generated
 */
public class ReTweetSoap implements Serializable {
	public static ReTweetSoap toSoapModel(ReTweet model) {
		ReTweetSoap soapModel = new ReTweetSoap();

		soapModel.setRetweetId(model.getRetweetId());
		soapModel.setTweetId(model.getTweetId());
		soapModel.setRetweet(model.getRetweet());
		soapModel.setIcon(model.getIcon());
		soapModel.setRetweetDate(model.getRetweetDate());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());

		return soapModel;
	}

	public static ReTweetSoap[] toSoapModels(ReTweet[] models) {
		ReTweetSoap[] soapModels = new ReTweetSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ReTweetSoap[][] toSoapModels(ReTweet[][] models) {
		ReTweetSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ReTweetSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ReTweetSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ReTweetSoap[] toSoapModels(List<ReTweet> models) {
		List<ReTweetSoap> soapModels = new ArrayList<ReTweetSoap>(models.size());

		for (ReTweet model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ReTweetSoap[soapModels.size()]);
	}

	public ReTweetSoap() {
	}

	public long getPrimaryKey() {
		return _retweetId;
	}

	public void setPrimaryKey(long pk) {
		setRetweetId(pk);
	}

	public long getRetweetId() {
		return _retweetId;
	}

	public void setRetweetId(long retweetId) {
		_retweetId = retweetId;
	}

	public long getTweetId() {
		return _tweetId;
	}

	public void setTweetId(long tweetId) {
		_tweetId = tweetId;
	}

	public String getRetweet() {
		return _retweet;
	}

	public void setRetweet(String retweet) {
		_retweet = retweet;
	}

	public String getIcon() {
		return _icon;
	}

	public void setIcon(String icon) {
		_icon = icon;
	}

	public Date getRetweetDate() {
		return _retweetDate;
	}

	public void setRetweetDate(Date retweetDate) {
		_retweetDate = retweetDate;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	private long _retweetId;
	private long _tweetId;
	private String _retweet;
	private String _icon;
	private Date _retweetDate;
	private Date _createDate;
	private Date _modifiedDate;
}