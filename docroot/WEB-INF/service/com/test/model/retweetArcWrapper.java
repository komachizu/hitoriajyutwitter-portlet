/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.test.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link retweetArc}.
 * </p>
 *
 * @author takuro
 * @see retweetArc
 * @generated
 */
public class retweetArcWrapper implements retweetArc, ModelWrapper<retweetArc> {
	public retweetArcWrapper(retweetArc retweetArc) {
		_retweetArc = retweetArc;
	}

	@Override
	public Class<?> getModelClass() {
		return retweetArc.class;
	}

	@Override
	public String getModelClassName() {
		return retweetArc.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("retweetId", getRetweetId());
		attributes.put("keyword", getKeyword());
		attributes.put("retweet", getRetweet());
		attributes.put("icon", getIcon());
		attributes.put("retweetDate", getRetweetDate());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long retweetId = (Long)attributes.get("retweetId");

		if (retweetId != null) {
			setRetweetId(retweetId);
		}

		String keyword = (String)attributes.get("keyword");

		if (keyword != null) {
			setKeyword(keyword);
		}

		String retweet = (String)attributes.get("retweet");

		if (retweet != null) {
			setRetweet(retweet);
		}

		String icon = (String)attributes.get("icon");

		if (icon != null) {
			setIcon(icon);
		}

		Date retweetDate = (Date)attributes.get("retweetDate");

		if (retweetDate != null) {
			setRetweetDate(retweetDate);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}
	}

	/**
	* Returns the primary key of this retweet arc.
	*
	* @return the primary key of this retweet arc
	*/
	@Override
	public com.test.service.persistence.retweetArcPK getPrimaryKey() {
		return _retweetArc.getPrimaryKey();
	}

	/**
	* Sets the primary key of this retweet arc.
	*
	* @param primaryKey the primary key of this retweet arc
	*/
	@Override
	public void setPrimaryKey(
		com.test.service.persistence.retweetArcPK primaryKey) {
		_retweetArc.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the retweet ID of this retweet arc.
	*
	* @return the retweet ID of this retweet arc
	*/
	@Override
	public long getRetweetId() {
		return _retweetArc.getRetweetId();
	}

	/**
	* Sets the retweet ID of this retweet arc.
	*
	* @param retweetId the retweet ID of this retweet arc
	*/
	@Override
	public void setRetweetId(long retweetId) {
		_retweetArc.setRetweetId(retweetId);
	}

	/**
	* Returns the keyword of this retweet arc.
	*
	* @return the keyword of this retweet arc
	*/
	@Override
	public java.lang.String getKeyword() {
		return _retweetArc.getKeyword();
	}

	/**
	* Sets the keyword of this retweet arc.
	*
	* @param keyword the keyword of this retweet arc
	*/
	@Override
	public void setKeyword(java.lang.String keyword) {
		_retweetArc.setKeyword(keyword);
	}

	/**
	* Returns the retweet of this retweet arc.
	*
	* @return the retweet of this retweet arc
	*/
	@Override
	public java.lang.String getRetweet() {
		return _retweetArc.getRetweet();
	}

	/**
	* Sets the retweet of this retweet arc.
	*
	* @param retweet the retweet of this retweet arc
	*/
	@Override
	public void setRetweet(java.lang.String retweet) {
		_retweetArc.setRetweet(retweet);
	}

	/**
	* Returns the icon of this retweet arc.
	*
	* @return the icon of this retweet arc
	*/
	@Override
	public java.lang.String getIcon() {
		return _retweetArc.getIcon();
	}

	/**
	* Sets the icon of this retweet arc.
	*
	* @param icon the icon of this retweet arc
	*/
	@Override
	public void setIcon(java.lang.String icon) {
		_retweetArc.setIcon(icon);
	}

	/**
	* Returns the retweet date of this retweet arc.
	*
	* @return the retweet date of this retweet arc
	*/
	@Override
	public java.util.Date getRetweetDate() {
		return _retweetArc.getRetweetDate();
	}

	/**
	* Sets the retweet date of this retweet arc.
	*
	* @param retweetDate the retweet date of this retweet arc
	*/
	@Override
	public void setRetweetDate(java.util.Date retweetDate) {
		_retweetArc.setRetweetDate(retweetDate);
	}

	/**
	* Returns the create date of this retweet arc.
	*
	* @return the create date of this retweet arc
	*/
	@Override
	public java.util.Date getCreateDate() {
		return _retweetArc.getCreateDate();
	}

	/**
	* Sets the create date of this retweet arc.
	*
	* @param createDate the create date of this retweet arc
	*/
	@Override
	public void setCreateDate(java.util.Date createDate) {
		_retweetArc.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this retweet arc.
	*
	* @return the modified date of this retweet arc
	*/
	@Override
	public java.util.Date getModifiedDate() {
		return _retweetArc.getModifiedDate();
	}

	/**
	* Sets the modified date of this retweet arc.
	*
	* @param modifiedDate the modified date of this retweet arc
	*/
	@Override
	public void setModifiedDate(java.util.Date modifiedDate) {
		_retweetArc.setModifiedDate(modifiedDate);
	}

	@Override
	public boolean isNew() {
		return _retweetArc.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_retweetArc.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _retweetArc.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_retweetArc.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _retweetArc.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _retweetArc.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_retweetArc.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _retweetArc.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_retweetArc.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_retweetArc.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_retweetArc.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new retweetArcWrapper((retweetArc)_retweetArc.clone());
	}

	@Override
	public int compareTo(com.test.model.retweetArc retweetArc) {
		return _retweetArc.compareTo(retweetArc);
	}

	@Override
	public int hashCode() {
		return _retweetArc.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.test.model.retweetArc> toCacheModel() {
		return _retweetArc.toCacheModel();
	}

	@Override
	public com.test.model.retweetArc toEscapedModel() {
		return new retweetArcWrapper(_retweetArc.toEscapedModel());
	}

	@Override
	public com.test.model.retweetArc toUnescapedModel() {
		return new retweetArcWrapper(_retweetArc.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _retweetArc.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _retweetArc.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_retweetArc.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof retweetArcWrapper)) {
			return false;
		}

		retweetArcWrapper retweetArcWrapper = (retweetArcWrapper)obj;

		if (Validator.equals(_retweetArc, retweetArcWrapper._retweetArc)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public retweetArc getWrappedretweetArc() {
		return _retweetArc;
	}

	@Override
	public retweetArc getWrappedModel() {
		return _retweetArc;
	}

	@Override
	public void resetOriginalValues() {
		_retweetArc.resetOriginalValues();
	}

	private retweetArc _retweetArc;
}