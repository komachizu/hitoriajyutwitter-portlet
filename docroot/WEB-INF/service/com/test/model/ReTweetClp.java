/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.test.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import com.test.service.ClpSerializer;
import com.test.service.ReTweetLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author takuro
 */
public class ReTweetClp extends BaseModelImpl<ReTweet> implements ReTweet {
	public ReTweetClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return ReTweet.class;
	}

	@Override
	public String getModelClassName() {
		return ReTweet.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _retweetId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setRetweetId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _retweetId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("retweetId", getRetweetId());
		attributes.put("tweetId", getTweetId());
		attributes.put("retweet", getRetweet());
		attributes.put("icon", getIcon());
		attributes.put("retweetDate", getRetweetDate());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long retweetId = (Long)attributes.get("retweetId");

		if (retweetId != null) {
			setRetweetId(retweetId);
		}

		Long tweetId = (Long)attributes.get("tweetId");

		if (tweetId != null) {
			setTweetId(tweetId);
		}

		String retweet = (String)attributes.get("retweet");

		if (retweet != null) {
			setRetweet(retweet);
		}

		String icon = (String)attributes.get("icon");

		if (icon != null) {
			setIcon(icon);
		}

		Date retweetDate = (Date)attributes.get("retweetDate");

		if (retweetDate != null) {
			setRetweetDate(retweetDate);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}
	}

	@Override
	public long getRetweetId() {
		return _retweetId;
	}

	@Override
	public void setRetweetId(long retweetId) {
		_retweetId = retweetId;

		if (_reTweetRemoteModel != null) {
			try {
				Class<?> clazz = _reTweetRemoteModel.getClass();

				Method method = clazz.getMethod("setRetweetId", long.class);

				method.invoke(_reTweetRemoteModel, retweetId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getTweetId() {
		return _tweetId;
	}

	@Override
	public void setTweetId(long tweetId) {
		_tweetId = tweetId;

		if (_reTweetRemoteModel != null) {
			try {
				Class<?> clazz = _reTweetRemoteModel.getClass();

				Method method = clazz.getMethod("setTweetId", long.class);

				method.invoke(_reTweetRemoteModel, tweetId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getRetweet() {
		return _retweet;
	}

	@Override
	public void setRetweet(String retweet) {
		_retweet = retweet;

		if (_reTweetRemoteModel != null) {
			try {
				Class<?> clazz = _reTweetRemoteModel.getClass();

				Method method = clazz.getMethod("setRetweet", String.class);

				method.invoke(_reTweetRemoteModel, retweet);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getIcon() {
		return _icon;
	}

	@Override
	public void setIcon(String icon) {
		_icon = icon;

		if (_reTweetRemoteModel != null) {
			try {
				Class<?> clazz = _reTweetRemoteModel.getClass();

				Method method = clazz.getMethod("setIcon", String.class);

				method.invoke(_reTweetRemoteModel, icon);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getRetweetDate() {
		return _retweetDate;
	}

	@Override
	public void setRetweetDate(Date retweetDate) {
		_retweetDate = retweetDate;

		if (_reTweetRemoteModel != null) {
			try {
				Class<?> clazz = _reTweetRemoteModel.getClass();

				Method method = clazz.getMethod("setRetweetDate", Date.class);

				method.invoke(_reTweetRemoteModel, retweetDate);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getCreateDate() {
		return _createDate;
	}

	@Override
	public void setCreateDate(Date createDate) {
		_createDate = createDate;

		if (_reTweetRemoteModel != null) {
			try {
				Class<?> clazz = _reTweetRemoteModel.getClass();

				Method method = clazz.getMethod("setCreateDate", Date.class);

				method.invoke(_reTweetRemoteModel, createDate);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getModifiedDate() {
		return _modifiedDate;
	}

	@Override
	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;

		if (_reTweetRemoteModel != null) {
			try {
				Class<?> clazz = _reTweetRemoteModel.getClass();

				Method method = clazz.getMethod("setModifiedDate", Date.class);

				method.invoke(_reTweetRemoteModel, modifiedDate);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getReTweetRemoteModel() {
		return _reTweetRemoteModel;
	}

	public void setReTweetRemoteModel(BaseModel<?> reTweetRemoteModel) {
		_reTweetRemoteModel = reTweetRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _reTweetRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_reTweetRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			ReTweetLocalServiceUtil.addReTweet(this);
		}
		else {
			ReTweetLocalServiceUtil.updateReTweet(this);
		}
	}

	@Override
	public ReTweet toEscapedModel() {
		return (ReTweet)ProxyUtil.newProxyInstance(ReTweet.class.getClassLoader(),
			new Class[] { ReTweet.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		ReTweetClp clone = new ReTweetClp();

		clone.setRetweetId(getRetweetId());
		clone.setTweetId(getTweetId());
		clone.setRetweet(getRetweet());
		clone.setIcon(getIcon());
		clone.setRetweetDate(getRetweetDate());
		clone.setCreateDate(getCreateDate());
		clone.setModifiedDate(getModifiedDate());

		return clone;
	}

	@Override
	public int compareTo(ReTweet reTweet) {
		int value = 0;

		value = DateUtil.compareTo(getRetweetDate(), reTweet.getRetweetDate());

		value = value * -1;

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ReTweetClp)) {
			return false;
		}

		ReTweetClp reTweet = (ReTweetClp)obj;

		long primaryKey = reTweet.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{retweetId=");
		sb.append(getRetweetId());
		sb.append(", tweetId=");
		sb.append(getTweetId());
		sb.append(", retweet=");
		sb.append(getRetweet());
		sb.append(", icon=");
		sb.append(getIcon());
		sb.append(", retweetDate=");
		sb.append(getRetweetDate());
		sb.append(", createDate=");
		sb.append(getCreateDate());
		sb.append(", modifiedDate=");
		sb.append(getModifiedDate());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(25);

		sb.append("<model><model-name>");
		sb.append("com.test.model.ReTweet");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>retweetId</column-name><column-value><![CDATA[");
		sb.append(getRetweetId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tweetId</column-name><column-value><![CDATA[");
		sb.append(getTweetId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>retweet</column-name><column-value><![CDATA[");
		sb.append(getRetweet());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>icon</column-name><column-value><![CDATA[");
		sb.append(getIcon());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>retweetDate</column-name><column-value><![CDATA[");
		sb.append(getRetweetDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>createDate</column-name><column-value><![CDATA[");
		sb.append(getCreateDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
		sb.append(getModifiedDate());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _retweetId;
	private long _tweetId;
	private String _retweet;
	private String _icon;
	private Date _retweetDate;
	private Date _createDate;
	private Date _modifiedDate;
	private BaseModel<?> _reTweetRemoteModel;
}