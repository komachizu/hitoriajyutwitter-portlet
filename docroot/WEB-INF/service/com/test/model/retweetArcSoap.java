/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.test.model;

import com.test.service.persistence.retweetArcPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.test.service.http.retweetArcServiceSoap}.
 *
 * @author takuro
 * @see com.test.service.http.retweetArcServiceSoap
 * @generated
 */
public class retweetArcSoap implements Serializable {
	public static retweetArcSoap toSoapModel(retweetArc model) {
		retweetArcSoap soapModel = new retweetArcSoap();

		soapModel.setRetweetId(model.getRetweetId());
		soapModel.setKeyword(model.getKeyword());
		soapModel.setRetweet(model.getRetweet());
		soapModel.setIcon(model.getIcon());
		soapModel.setRetweetDate(model.getRetweetDate());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());

		return soapModel;
	}

	public static retweetArcSoap[] toSoapModels(retweetArc[] models) {
		retweetArcSoap[] soapModels = new retweetArcSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static retweetArcSoap[][] toSoapModels(retweetArc[][] models) {
		retweetArcSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new retweetArcSoap[models.length][models[0].length];
		}
		else {
			soapModels = new retweetArcSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static retweetArcSoap[] toSoapModels(List<retweetArc> models) {
		List<retweetArcSoap> soapModels = new ArrayList<retweetArcSoap>(models.size());

		for (retweetArc model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new retweetArcSoap[soapModels.size()]);
	}

	public retweetArcSoap() {
	}

	public retweetArcPK getPrimaryKey() {
		return new retweetArcPK(_retweetId, _keyword);
	}

	public void setPrimaryKey(retweetArcPK pk) {
		setRetweetId(pk.retweetId);
		setKeyword(pk.keyword);
	}

	public long getRetweetId() {
		return _retweetId;
	}

	public void setRetweetId(long retweetId) {
		_retweetId = retweetId;
	}

	public String getKeyword() {
		return _keyword;
	}

	public void setKeyword(String keyword) {
		_keyword = keyword;
	}

	public String getRetweet() {
		return _retweet;
	}

	public void setRetweet(String retweet) {
		_retweet = retweet;
	}

	public String getIcon() {
		return _icon;
	}

	public void setIcon(String icon) {
		_icon = icon;
	}

	public Date getRetweetDate() {
		return _retweetDate;
	}

	public void setRetweetDate(Date retweetDate) {
		_retweetDate = retweetDate;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	private long _retweetId;
	private String _keyword;
	private String _retweet;
	private String _icon;
	private Date _retweetDate;
	private Date _createDate;
	private Date _modifiedDate;
}