/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.test.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ReTweet}.
 * </p>
 *
 * @author takuro
 * @see ReTweet
 * @generated
 */
public class ReTweetWrapper implements ReTweet, ModelWrapper<ReTweet> {
	public ReTweetWrapper(ReTweet reTweet) {
		_reTweet = reTweet;
	}

	@Override
	public Class<?> getModelClass() {
		return ReTweet.class;
	}

	@Override
	public String getModelClassName() {
		return ReTweet.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("retweetId", getRetweetId());
		attributes.put("tweetId", getTweetId());
		attributes.put("retweet", getRetweet());
		attributes.put("icon", getIcon());
		attributes.put("retweetDate", getRetweetDate());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long retweetId = (Long)attributes.get("retweetId");

		if (retweetId != null) {
			setRetweetId(retweetId);
		}

		Long tweetId = (Long)attributes.get("tweetId");

		if (tweetId != null) {
			setTweetId(tweetId);
		}

		String retweet = (String)attributes.get("retweet");

		if (retweet != null) {
			setRetweet(retweet);
		}

		String icon = (String)attributes.get("icon");

		if (icon != null) {
			setIcon(icon);
		}

		Date retweetDate = (Date)attributes.get("retweetDate");

		if (retweetDate != null) {
			setRetweetDate(retweetDate);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}
	}

	/**
	* Returns the primary key of this re tweet.
	*
	* @return the primary key of this re tweet
	*/
	@Override
	public long getPrimaryKey() {
		return _reTweet.getPrimaryKey();
	}

	/**
	* Sets the primary key of this re tweet.
	*
	* @param primaryKey the primary key of this re tweet
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_reTweet.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the retweet ID of this re tweet.
	*
	* @return the retweet ID of this re tweet
	*/
	@Override
	public long getRetweetId() {
		return _reTweet.getRetweetId();
	}

	/**
	* Sets the retweet ID of this re tweet.
	*
	* @param retweetId the retweet ID of this re tweet
	*/
	@Override
	public void setRetweetId(long retweetId) {
		_reTweet.setRetweetId(retweetId);
	}

	/**
	* Returns the tweet ID of this re tweet.
	*
	* @return the tweet ID of this re tweet
	*/
	@Override
	public long getTweetId() {
		return _reTweet.getTweetId();
	}

	/**
	* Sets the tweet ID of this re tweet.
	*
	* @param tweetId the tweet ID of this re tweet
	*/
	@Override
	public void setTweetId(long tweetId) {
		_reTweet.setTweetId(tweetId);
	}

	/**
	* Returns the retweet of this re tweet.
	*
	* @return the retweet of this re tweet
	*/
	@Override
	public java.lang.String getRetweet() {
		return _reTweet.getRetweet();
	}

	/**
	* Sets the retweet of this re tweet.
	*
	* @param retweet the retweet of this re tweet
	*/
	@Override
	public void setRetweet(java.lang.String retweet) {
		_reTweet.setRetweet(retweet);
	}

	/**
	* Returns the icon of this re tweet.
	*
	* @return the icon of this re tweet
	*/
	@Override
	public java.lang.String getIcon() {
		return _reTweet.getIcon();
	}

	/**
	* Sets the icon of this re tweet.
	*
	* @param icon the icon of this re tweet
	*/
	@Override
	public void setIcon(java.lang.String icon) {
		_reTweet.setIcon(icon);
	}

	/**
	* Returns the retweet date of this re tweet.
	*
	* @return the retweet date of this re tweet
	*/
	@Override
	public java.util.Date getRetweetDate() {
		return _reTweet.getRetweetDate();
	}

	/**
	* Sets the retweet date of this re tweet.
	*
	* @param retweetDate the retweet date of this re tweet
	*/
	@Override
	public void setRetweetDate(java.util.Date retweetDate) {
		_reTweet.setRetweetDate(retweetDate);
	}

	/**
	* Returns the create date of this re tweet.
	*
	* @return the create date of this re tweet
	*/
	@Override
	public java.util.Date getCreateDate() {
		return _reTweet.getCreateDate();
	}

	/**
	* Sets the create date of this re tweet.
	*
	* @param createDate the create date of this re tweet
	*/
	@Override
	public void setCreateDate(java.util.Date createDate) {
		_reTweet.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this re tweet.
	*
	* @return the modified date of this re tweet
	*/
	@Override
	public java.util.Date getModifiedDate() {
		return _reTweet.getModifiedDate();
	}

	/**
	* Sets the modified date of this re tweet.
	*
	* @param modifiedDate the modified date of this re tweet
	*/
	@Override
	public void setModifiedDate(java.util.Date modifiedDate) {
		_reTweet.setModifiedDate(modifiedDate);
	}

	@Override
	public boolean isNew() {
		return _reTweet.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_reTweet.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _reTweet.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_reTweet.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _reTweet.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _reTweet.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_reTweet.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _reTweet.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_reTweet.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_reTweet.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_reTweet.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ReTweetWrapper((ReTweet)_reTweet.clone());
	}

	@Override
	public int compareTo(com.test.model.ReTweet reTweet) {
		return _reTweet.compareTo(reTweet);
	}

	@Override
	public int hashCode() {
		return _reTweet.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.test.model.ReTweet> toCacheModel() {
		return _reTweet.toCacheModel();
	}

	@Override
	public com.test.model.ReTweet toEscapedModel() {
		return new ReTweetWrapper(_reTweet.toEscapedModel());
	}

	@Override
	public com.test.model.ReTweet toUnescapedModel() {
		return new ReTweetWrapper(_reTweet.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _reTweet.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _reTweet.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_reTweet.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ReTweetWrapper)) {
			return false;
		}

		ReTweetWrapper reTweetWrapper = (ReTweetWrapper)obj;

		if (Validator.equals(_reTweet, reTweetWrapper._reTweet)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public ReTweet getWrappedReTweet() {
		return _reTweet;
	}

	@Override
	public ReTweet getWrappedModel() {
		return _reTweet;
	}

	@Override
	public void resetOriginalValues() {
		_reTweet.resetOriginalValues();
	}

	private ReTweet _reTweet;
}