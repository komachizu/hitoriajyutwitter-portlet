/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.test.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import com.test.model.retweetArc;

import java.util.List;

/**
 * The persistence utility for the retweet arc service. This utility wraps {@link retweetArcPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author takuro
 * @see retweetArcPersistence
 * @see retweetArcPersistenceImpl
 * @generated
 */
public class retweetArcUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(retweetArc retweetArc) {
		getPersistence().clearCache(retweetArc);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<retweetArc> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<retweetArc> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<retweetArc> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static retweetArc update(retweetArc retweetArc)
		throws SystemException {
		return getPersistence().update(retweetArc);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static retweetArc update(retweetArc retweetArc,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(retweetArc, serviceContext);
	}

	/**
	* Returns all the retweet arcs where keyword = &#63;.
	*
	* @param keyword the keyword
	* @return the matching retweet arcs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.test.model.retweetArc> findByKeyword(
		java.lang.String keyword)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByKeyword(keyword);
	}

	/**
	* Returns a range of all the retweet arcs where keyword = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.test.model.impl.retweetArcModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param keyword the keyword
	* @param start the lower bound of the range of retweet arcs
	* @param end the upper bound of the range of retweet arcs (not inclusive)
	* @return the range of matching retweet arcs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.test.model.retweetArc> findByKeyword(
		java.lang.String keyword, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByKeyword(keyword, start, end);
	}

	/**
	* Returns an ordered range of all the retweet arcs where keyword = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.test.model.impl.retweetArcModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param keyword the keyword
	* @param start the lower bound of the range of retweet arcs
	* @param end the upper bound of the range of retweet arcs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching retweet arcs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.test.model.retweetArc> findByKeyword(
		java.lang.String keyword, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByKeyword(keyword, start, end, orderByComparator);
	}

	/**
	* Returns the first retweet arc in the ordered set where keyword = &#63;.
	*
	* @param keyword the keyword
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching retweet arc
	* @throws com.test.NoSuchretweetArcException if a matching retweet arc could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.test.model.retweetArc findByKeyword_First(
		java.lang.String keyword,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.test.NoSuchretweetArcException {
		return getPersistence().findByKeyword_First(keyword, orderByComparator);
	}

	/**
	* Returns the first retweet arc in the ordered set where keyword = &#63;.
	*
	* @param keyword the keyword
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching retweet arc, or <code>null</code> if a matching retweet arc could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.test.model.retweetArc fetchByKeyword_First(
		java.lang.String keyword,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByKeyword_First(keyword, orderByComparator);
	}

	/**
	* Returns the last retweet arc in the ordered set where keyword = &#63;.
	*
	* @param keyword the keyword
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching retweet arc
	* @throws com.test.NoSuchretweetArcException if a matching retweet arc could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.test.model.retweetArc findByKeyword_Last(
		java.lang.String keyword,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.test.NoSuchretweetArcException {
		return getPersistence().findByKeyword_Last(keyword, orderByComparator);
	}

	/**
	* Returns the last retweet arc in the ordered set where keyword = &#63;.
	*
	* @param keyword the keyword
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching retweet arc, or <code>null</code> if a matching retweet arc could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.test.model.retweetArc fetchByKeyword_Last(
		java.lang.String keyword,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByKeyword_Last(keyword, orderByComparator);
	}

	/**
	* Returns the retweet arcs before and after the current retweet arc in the ordered set where keyword = &#63;.
	*
	* @param retweetArcPK the primary key of the current retweet arc
	* @param keyword the keyword
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next retweet arc
	* @throws com.test.NoSuchretweetArcException if a retweet arc with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.test.model.retweetArc[] findByKeyword_PrevAndNext(
		com.test.service.persistence.retweetArcPK retweetArcPK,
		java.lang.String keyword,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.test.NoSuchretweetArcException {
		return getPersistence()
				   .findByKeyword_PrevAndNext(retweetArcPK, keyword,
			orderByComparator);
	}

	/**
	* Removes all the retweet arcs where keyword = &#63; from the database.
	*
	* @param keyword the keyword
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByKeyword(java.lang.String keyword)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByKeyword(keyword);
	}

	/**
	* Returns the number of retweet arcs where keyword = &#63;.
	*
	* @param keyword the keyword
	* @return the number of matching retweet arcs
	* @throws SystemException if a system exception occurred
	*/
	public static int countByKeyword(java.lang.String keyword)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByKeyword(keyword);
	}

	/**
	* Caches the retweet arc in the entity cache if it is enabled.
	*
	* @param retweetArc the retweet arc
	*/
	public static void cacheResult(com.test.model.retweetArc retweetArc) {
		getPersistence().cacheResult(retweetArc);
	}

	/**
	* Caches the retweet arcs in the entity cache if it is enabled.
	*
	* @param retweetArcs the retweet arcs
	*/
	public static void cacheResult(
		java.util.List<com.test.model.retweetArc> retweetArcs) {
		getPersistence().cacheResult(retweetArcs);
	}

	/**
	* Creates a new retweet arc with the primary key. Does not add the retweet arc to the database.
	*
	* @param retweetArcPK the primary key for the new retweet arc
	* @return the new retweet arc
	*/
	public static com.test.model.retweetArc create(
		com.test.service.persistence.retweetArcPK retweetArcPK) {
		return getPersistence().create(retweetArcPK);
	}

	/**
	* Removes the retweet arc with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param retweetArcPK the primary key of the retweet arc
	* @return the retweet arc that was removed
	* @throws com.test.NoSuchretweetArcException if a retweet arc with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.test.model.retweetArc remove(
		com.test.service.persistence.retweetArcPK retweetArcPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.test.NoSuchretweetArcException {
		return getPersistence().remove(retweetArcPK);
	}

	public static com.test.model.retweetArc updateImpl(
		com.test.model.retweetArc retweetArc)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(retweetArc);
	}

	/**
	* Returns the retweet arc with the primary key or throws a {@link com.test.NoSuchretweetArcException} if it could not be found.
	*
	* @param retweetArcPK the primary key of the retweet arc
	* @return the retweet arc
	* @throws com.test.NoSuchretweetArcException if a retweet arc with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.test.model.retweetArc findByPrimaryKey(
		com.test.service.persistence.retweetArcPK retweetArcPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.test.NoSuchretweetArcException {
		return getPersistence().findByPrimaryKey(retweetArcPK);
	}

	/**
	* Returns the retweet arc with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param retweetArcPK the primary key of the retweet arc
	* @return the retweet arc, or <code>null</code> if a retweet arc with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.test.model.retweetArc fetchByPrimaryKey(
		com.test.service.persistence.retweetArcPK retweetArcPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(retweetArcPK);
	}

	/**
	* Returns all the retweet arcs.
	*
	* @return the retweet arcs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.test.model.retweetArc> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the retweet arcs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.test.model.impl.retweetArcModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of retweet arcs
	* @param end the upper bound of the range of retweet arcs (not inclusive)
	* @return the range of retweet arcs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.test.model.retweetArc> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the retweet arcs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.test.model.impl.retweetArcModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of retweet arcs
	* @param end the upper bound of the range of retweet arcs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of retweet arcs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.test.model.retweetArc> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the retweet arcs from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of retweet arcs.
	*
	* @return the number of retweet arcs
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static retweetArcPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (retweetArcPersistence)PortletBeanLocatorUtil.locate(com.test.service.ClpSerializer.getServletContextName(),
					retweetArcPersistence.class.getName());

			ReferenceRegistry.registerReference(retweetArcUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(retweetArcPersistence persistence) {
	}

	private static retweetArcPersistence _persistence;
}