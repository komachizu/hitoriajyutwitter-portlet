/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.test.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import com.test.model.ReTweet;

/**
 * The persistence interface for the re tweet service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author takuro
 * @see ReTweetPersistenceImpl
 * @see ReTweetUtil
 * @generated
 */
public interface ReTweetPersistence extends BasePersistence<ReTweet> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ReTweetUtil} to access the re tweet persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the re tweets where tweetId = &#63;.
	*
	* @param tweetId the tweet ID
	* @return the matching re tweets
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.test.model.ReTweet> findByTweetId(long tweetId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the re tweets where tweetId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.test.model.impl.ReTweetModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param tweetId the tweet ID
	* @param start the lower bound of the range of re tweets
	* @param end the upper bound of the range of re tweets (not inclusive)
	* @return the range of matching re tweets
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.test.model.ReTweet> findByTweetId(long tweetId,
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the re tweets where tweetId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.test.model.impl.ReTweetModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param tweetId the tweet ID
	* @param start the lower bound of the range of re tweets
	* @param end the upper bound of the range of re tweets (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching re tweets
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.test.model.ReTweet> findByTweetId(long tweetId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first re tweet in the ordered set where tweetId = &#63;.
	*
	* @param tweetId the tweet ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching re tweet
	* @throws com.test.NoSuchReTweetException if a matching re tweet could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.test.model.ReTweet findByTweetId_First(long tweetId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.test.NoSuchReTweetException;

	/**
	* Returns the first re tweet in the ordered set where tweetId = &#63;.
	*
	* @param tweetId the tweet ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching re tweet, or <code>null</code> if a matching re tweet could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.test.model.ReTweet fetchByTweetId_First(long tweetId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last re tweet in the ordered set where tweetId = &#63;.
	*
	* @param tweetId the tweet ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching re tweet
	* @throws com.test.NoSuchReTweetException if a matching re tweet could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.test.model.ReTweet findByTweetId_Last(long tweetId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.test.NoSuchReTweetException;

	/**
	* Returns the last re tweet in the ordered set where tweetId = &#63;.
	*
	* @param tweetId the tweet ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching re tweet, or <code>null</code> if a matching re tweet could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.test.model.ReTweet fetchByTweetId_Last(long tweetId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the re tweets before and after the current re tweet in the ordered set where tweetId = &#63;.
	*
	* @param retweetId the primary key of the current re tweet
	* @param tweetId the tweet ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next re tweet
	* @throws com.test.NoSuchReTweetException if a re tweet with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.test.model.ReTweet[] findByTweetId_PrevAndNext(long retweetId,
		long tweetId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.test.NoSuchReTweetException;

	/**
	* Removes all the re tweets where tweetId = &#63; from the database.
	*
	* @param tweetId the tweet ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByTweetId(long tweetId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of re tweets where tweetId = &#63;.
	*
	* @param tweetId the tweet ID
	* @return the number of matching re tweets
	* @throws SystemException if a system exception occurred
	*/
	public int countByTweetId(long tweetId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the re tweet in the entity cache if it is enabled.
	*
	* @param reTweet the re tweet
	*/
	public void cacheResult(com.test.model.ReTweet reTweet);

	/**
	* Caches the re tweets in the entity cache if it is enabled.
	*
	* @param reTweets the re tweets
	*/
	public void cacheResult(java.util.List<com.test.model.ReTweet> reTweets);

	/**
	* Creates a new re tweet with the primary key. Does not add the re tweet to the database.
	*
	* @param retweetId the primary key for the new re tweet
	* @return the new re tweet
	*/
	public com.test.model.ReTweet create(long retweetId);

	/**
	* Removes the re tweet with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param retweetId the primary key of the re tweet
	* @return the re tweet that was removed
	* @throws com.test.NoSuchReTweetException if a re tweet with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.test.model.ReTweet remove(long retweetId)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.test.NoSuchReTweetException;

	public com.test.model.ReTweet updateImpl(com.test.model.ReTweet reTweet)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the re tweet with the primary key or throws a {@link com.test.NoSuchReTweetException} if it could not be found.
	*
	* @param retweetId the primary key of the re tweet
	* @return the re tweet
	* @throws com.test.NoSuchReTweetException if a re tweet with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.test.model.ReTweet findByPrimaryKey(long retweetId)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.test.NoSuchReTweetException;

	/**
	* Returns the re tweet with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param retweetId the primary key of the re tweet
	* @return the re tweet, or <code>null</code> if a re tweet with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.test.model.ReTweet fetchByPrimaryKey(long retweetId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the re tweets.
	*
	* @return the re tweets
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.test.model.ReTweet> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the re tweets.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.test.model.impl.ReTweetModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of re tweets
	* @param end the upper bound of the range of re tweets (not inclusive)
	* @return the range of re tweets
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.test.model.ReTweet> findAll(int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the re tweets.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.test.model.impl.ReTweetModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of re tweets
	* @param end the upper bound of the range of re tweets (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of re tweets
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.test.model.ReTweet> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the re tweets from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of re tweets.
	*
	* @return the number of re tweets
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}