/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.test.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ReTweetLocalService}.
 *
 * @author takuro
 * @see ReTweetLocalService
 * @generated
 */
public class ReTweetLocalServiceWrapper implements ReTweetLocalService,
	ServiceWrapper<ReTweetLocalService> {
	public ReTweetLocalServiceWrapper(ReTweetLocalService reTweetLocalService) {
		_reTweetLocalService = reTweetLocalService;
	}

	/**
	* Adds the re tweet to the database. Also notifies the appropriate model listeners.
	*
	* @param reTweet the re tweet
	* @return the re tweet that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.test.model.ReTweet addReTweet(com.test.model.ReTweet reTweet)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _reTweetLocalService.addReTweet(reTweet);
	}

	/**
	* Creates a new re tweet with the primary key. Does not add the re tweet to the database.
	*
	* @param retweetId the primary key for the new re tweet
	* @return the new re tweet
	*/
	@Override
	public com.test.model.ReTweet createReTweet(long retweetId) {
		return _reTweetLocalService.createReTweet(retweetId);
	}

	/**
	* Deletes the re tweet with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param retweetId the primary key of the re tweet
	* @return the re tweet that was removed
	* @throws PortalException if a re tweet with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.test.model.ReTweet deleteReTweet(long retweetId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _reTweetLocalService.deleteReTweet(retweetId);
	}

	/**
	* Deletes the re tweet from the database. Also notifies the appropriate model listeners.
	*
	* @param reTweet the re tweet
	* @return the re tweet that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.test.model.ReTweet deleteReTweet(com.test.model.ReTweet reTweet)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _reTweetLocalService.deleteReTweet(reTweet);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _reTweetLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _reTweetLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.test.model.impl.ReTweetModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _reTweetLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.test.model.impl.ReTweetModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _reTweetLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _reTweetLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _reTweetLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public com.test.model.ReTweet fetchReTweet(long retweetId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _reTweetLocalService.fetchReTweet(retweetId);
	}

	/**
	* Returns the re tweet with the primary key.
	*
	* @param retweetId the primary key of the re tweet
	* @return the re tweet
	* @throws PortalException if a re tweet with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.test.model.ReTweet getReTweet(long retweetId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _reTweetLocalService.getReTweet(retweetId);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _reTweetLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the re tweets.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.test.model.impl.ReTweetModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of re tweets
	* @param end the upper bound of the range of re tweets (not inclusive)
	* @return the range of re tweets
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<com.test.model.ReTweet> getReTweets(int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _reTweetLocalService.getReTweets(start, end);
	}

	/**
	* Returns the number of re tweets.
	*
	* @return the number of re tweets
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getReTweetsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _reTweetLocalService.getReTweetsCount();
	}

	/**
	* Updates the re tweet in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param reTweet the re tweet
	* @return the re tweet that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.test.model.ReTweet updateReTweet(com.test.model.ReTweet reTweet)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _reTweetLocalService.updateReTweet(reTweet);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _reTweetLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_reTweetLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _reTweetLocalService.invokeMethod(name, parameterTypes, arguments);
	}

	@Override
	public java.util.List<com.test.model.ReTweet> getReTweetList(long tweetId) {
		return _reTweetLocalService.getReTweetList(tweetId);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public ReTweetLocalService getWrappedReTweetLocalService() {
		return _reTweetLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedReTweetLocalService(
		ReTweetLocalService reTweetLocalService) {
		_reTweetLocalService = reTweetLocalService;
	}

	@Override
	public ReTweetLocalService getWrappedService() {
		return _reTweetLocalService;
	}

	@Override
	public void setWrappedService(ReTweetLocalService reTweetLocalService) {
		_reTweetLocalService = reTweetLocalService;
	}

	private ReTweetLocalService _reTweetLocalService;
}